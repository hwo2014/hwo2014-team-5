// Simplified HWO physics engine
var Vec2 = require('./Vec2.js').Vec2;

var engine = function() {

  this.friction = 0.000;
  this.power = 10.0;
  this.turboFactor = 1.0;

  this.car = {
    vel: 0.0,
    acc: 0.0,
    ang: 0.0
  }

  this.track = new Vec2(1, 0);
  this.i = new Vec2(1, 0);
  this.j = new Vec2(0, 1);

  this.step = 0;

this.check = [
  {tick: 1, acc: 0, vel: 0},
{tick: 2, acc: 0, vel: 0},
{tick: 3, acc: 0, vel: 0},
{tick: 4, acc: 0.1, vel: 0.1},
{tick: 5, acc: 0.09800000000000009, vel: 0.1980000000000001},
{tick: 6, acc: 0.09603999999999993, vel: 0.29404},
{tick: 7, acc: 0.09411920000000012, vel: 0.38815920000000015},
{tick: 8, acc: 0.09223681599999956, vel: 0.4803960159999997},
{tick: 9, acc: 0.09039207968000029, vel: 0.57078809568},
{tick: 10, acc: 0.08858423808640015, vel: 0.6593723337664001},
{tick: 11, acc: 0.08681255332467197, vel: 0.7461848870910721},
{tick: 12, acc: 0.08507630225817797, vel: 0.8312611893492501},
{tick: 13, acc: 0.08337477621301437, vel: 0.9146359655622645},
{tick: 14, acc: 0.08170728068875643, vel: 0.9963432462510209},
{tick: 15, acc: 0.08007313507497837, vel: 1.0764163813259993},
{tick: 16, acc: 0.07847167237348263, vel: 1.1548880536994819},
{tick: 17, acc: 0.07690223892600301, vel: 1.231790292625485},
{tick: 18, acc: 0.07536419414750029, vel: 1.3071544867729852},
{tick: 19, acc: 0.07385691026453323, vel: 1.3810113970375184},
{tick: 20, acc: 0.07237977205925539, vel: 1.4533911690967738},
{tick: 21, acc: 0.07093217661805973, vel: 1.5243233457148335},
{tick: 22, acc: 0.06951353308569885, vel: 1.5938368788005324},
{tick: 23, acc: 0.06812326242399713, vel: 1.6619601412245295},
{tick: 24, acc: 0.0667607971755082, vel: 1.7287209384000377},
{tick: 25, acc: 0.06542558123199527, vel: 1.794146519632033},
{tick: 26, acc: 0.06411706960736652, vel: 1.8582635892393995},
{tick: 27, acc: 0.0628347282152042, vel: 1.9210983174546037},
{tick: 28, acc: 0.061578033650913966, vel: 1.9826763511055177},
{tick: 29, acc: 0.06034647297789064, vel: 2.0430228240834083},
{tick: 30, acc: 0.059139543518323734, vel: 2.102162367601732},
{tick: 31, acc: 0.05795675264795719, vel: 2.160119120249689},
{tick: 32, acc: 0.05679761759503066, vel: 2.21691673784472},
{tick: 33, acc: 0.05566166524309324, vel: 2.272578403087813},
{tick: 34, acc: 0.054548431938236774, vel: 2.32712683502605},
{tick: 35, acc: 0.05345746329948753, vel: 2.3805842983255374},
{tick: 36, acc: 0.052388314033500194, vel: 2.4329726123590376},
{tick: 37, acc: 0.05134054775280816, vel: 2.4843131601118458},
{tick: 38, acc: 0.05031373679774731, vel: 2.534626896909593},
{tick: 39, acc: 0.04930746206181169, vel: 2.5839343589714048},
{tick: 40, acc: 0.04832131282059038, vel: 2.632255671791995},
{tick: 41, acc: 0.04735488656417175, vel: 2.679610558356167},
{tick: 42, acc: 0.04640778883285179, vel: 2.7260183471890187},
{tick: 43, acc: 0.0454796330562246, vel: 2.7714979802452433},
{tick: 44, acc: 0.04457004039511503, vel: 2.8160680206403583},
{tick: 45, acc: 0.043678639587156454, vel: 2.859746660227515},
{tick: 46, acc: 0.042805066795480684, vel: 2.9025517270229955},
{tick: 47, acc: 0.04194896545952531, vel: 2.9445006924825208},
{tick: 48, acc: 0.04110998615030326, vel: 2.985610678632824},
{tick: 49, acc: 0.04028778642739894, vel: 3.025898465060223},
{tick: 50, acc: 0.039482030698806625, vel: 3.0653804957590296},
{tick: 51, acc: 0.03869239008480463, vel: 3.1040728858438342},
{tick: 52, acc: 0.03791854228313696, vel: 3.141991428126971},
{tick: 53, acc: 0.03716017143744921, vel: 3.1791515995644204},
{tick: 54, acc: 0.03641696800870875, vel: 3.215568567573129},
{tick: 55, acc: 0.035688628648514964, vel: 3.251257196221644},
{tick: 56, acc: 0.03497485607558559, vel: 3.2862320522972297},
{tick: 57, acc: 0.03427535895405498, vel: 3.3205074112512847},
{tick: 58, acc: 0.033589851774976864, vel: 3.3540972630262615},
{tick: 59, acc: 0.032918054739473845, vel: 3.3870153177657354},
{tick: 60, acc: 0.03225969364467929, vel: 3.4192750114104147}
];

}


engine.prototype.run = function( throttle ) {

  // acceleration equals: power*throttle / x^2
  this.car.vel += this.car.acc;
  var foo = 0;
  var fixTerm = 1; //1 / ((Math.PI*Math.PI) / 6);
  //this.car.acc = (throttle * this.power * fixTerm) / ((this.step+foo) + (this.step+foo)*(this.step+foo));
  this.car.vel = (throttle * this.power) * (1 / (1 + Math.E^(-1*throttle*this.step)));
  this.car.vel = (throttle * this.power) * ((1 / (1 + Math.pow(Math.E, (-1*1.0*this.step)))) - 0.5);
  // oletus: kullakin throttlen arvolla rajanopeus
  // oletus: lineaarinen tai x^2 funktio


  // throttle affects car engine that produces
  // acceleration to the direction of track.

  // throttle increases acceleration, but engine power depends on velocity
  //this.car.acc = throttle*this.power / (this.car.vel + 0.000000001 ) - this.friction;
  //this.car.acc -= (1.0 - throttle ) * this.power / (this.car.vel + 0.000000001 );
  // one way to implement terminal velocity
  //if (this.car.acc < 0.006) this.car.acc = 0;
  
  // friction decreases acceleration
  //this.car.acc -= this.friction;

  // speed: 7.0 -> 1.0 in 290 ticks
  // throttle: 0.0

  // acceleration alters velocity

  // we NEVER have negative velocity, so if we
  // result such, we zero all movement.
  // if ( this.car.vel < 0 ) {
  //   this.car.vel = 0.0;
  //   this.car.acc = 0.0;
  // }

}

engine.prototype.advance = function( s, t ) {
  for ( var i = 0; i < s; i++ ) {
    this.run( t );
    console.log('%s, carvel: %s, vel: %s', i, this.car.vel, this.check[i].vel - this.car.vel);
    //console.log('%s, vel:%s', this.step, this.car.vel);
    this.step += 0.1;
  }
  //console.log(e.vt(1.0));
  // for ( var i = 0; i <= 1.0; i+=0.1) {
  //   console.log('%s, %s', i, e.vt(i));
  // }
}



// test
var e = new engine();
e.advance( 40 , 0.5 );
//e.advance( 150, 0.8 );
//e.advance( 20 , 0.5 );
