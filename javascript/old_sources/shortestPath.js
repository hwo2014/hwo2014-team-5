// Floyd–Warshall algorithm for finding the shortest path for any track


// TODO: Make sure lanes are sorted by index

// Keimola's lanes
var lanes = [
  {'distanceFromCenter': -10, 'index': 0},
  {'distanceFromCenter': 10, 'index': 1}
];

// Keimola's pieces
var pieces = [
  {'length': 100},
  {'length': 100},
  {'length': 100},
  {'length': 100, 'switch': true},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 22.5, 'radius': 200, 'switch': true},
  {'length': 100},
  {'length': 100},
  {'angle': -22.5, 'radius': 200},
  {'length': 100},
  {'length': 100, 'switch': true},
  {'angle': -45, 'radius': 100},
  {'angle': -45, 'radius': 100},
  {'angle': -45, 'radius': 100},
  {'angle': -45, 'radius': 100},
  {'length': 100, 'switch': true},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 22.5, 'radius': 200},
  {'angle': -22.5, 'radius': 200},
  {'length': 100, 'switch': true},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'length': 62},
  {'angle': -45, 'radius': 100, 'switch': true},
  {'angle': -45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'angle': 45, 'radius': 100},
  {'length': 100, 'switch': true},
  {'length': 100},
  {'length': 100},
  {'length': 100},
  {'length': 90}
];

function graphEdge(weight, to) {
  this.weight = weight;
  this.toNode = to;
  this.isLeft = false;
  this.isRight = false;
}

graphEdge.prototype = {
  setToNode: function(node) {
    this.toNode = node;
  }
};

function graphNode(laneIdx) {
  this.edges = [];
  this.laneIndex = laneIdx;
  this.left = null;
  this.right = null;
  this.forward = null;
}

// graphNode.prototype = {

//   // Add possible path
//   addEdge: function(edge) {

//   }
// };

var allNodes = [],        // To store all nodes
    startNodes = [],      // To store starting nodes
    laneNodes = [],       // To store current nodes per lane
    prevLaneNodes = [];   // To store prev nodes per lane


var calcWeights = function(edge, piece, laneIdx) {


  var neighbourIdx = null;

  if (edge.isLeft || edge.isRight) {
    neighbourIdx += edge.isLeft ? laneIdx-1 : laneIdx+1;
  }

  // Straight
  if (piece.length) {
    var laneDist;
    if (neighbourIdx) {
      laneDist = Math.abs(lanes[neighbourIdx].distanceFromCenter - lanes[laneIdx].distanceFromCenter);
      edge.weight += Math.sqrt(Math.pow(laneDist, 2) + Math.pow(piece.length, 2));
    } else {
      edge.weight += piece.length;
    }
        
  }

  // Bend
  if (piece.angle) {

    var arcLen, arcLen2;
    var ang = Math.abs(piece.angle);
    var direction = ang ? (piece.angle / ang) : 1;
    var r = piece.radius - direction * lanes[laneIdx].distanceFromCenter;
    arcLen = (ang / 360) * 2 * Math.PI * r;

    // Average between two curve lengths
    if (neighbourIdx) {

      r = piece.radius - direction * lanes[neighbourIdx].distanceFromCenter;
      arcLen2 = (ang / 360) * 2 * Math.PI * r;
      edge.weight += (arcLen + arcLen2) / 2;


    } else {
      edge.weight += arcLen;
    }

  }

};



var buildGraph = function() {

  var currentPiece = null,
      currentNode = null,
      prevNode = null,
      currentEdge = null,
      isStart = false,
      isSwitch = false,
      wasSwitch = false;

  for (var i = 0; i < pieces.length; ++i) {

    currentPiece = pieces[i];

    // Check for a switch
    isSwitch = currentPiece.switch ? true : false;

    // Init
    isStart = i === 0;

    for (var laneIdx = 0; laneIdx < lanes.length; ++laneIdx) {

      // console.log('laneIdx', laneIdx);
      // console.log('laneNodes.length', laneNodes.length);
      // console.log('lanes.length', lanes.length);

      if (isStart) {
        currentNode = new graphNode(laneIdx);
        allNodes.push(currentNode);
        laneNodes.push(currentNode);
        startNodes.push(currentNode);
        
      } else {
        currentNode = laneNodes[laneIdx];
      }
      
      // There must be a forward edge
      if (!currentNode.forward) {
        currentNode.forward = new graphEdge(0, null);
        currentNode.edges.push(currentNode.forward);
      }

      // Add weights to edges
      for (var k = 0; k < currentNode.edges.length; ++k) {

        currentEdge = currentNode.edges[k];

        calcWeights(currentEdge, currentPiece, laneIdx);
        
      }

      if (isSwitch) {

        // Create new node and set prevNodes edge
        prevNode = laneNodes[laneIdx];
        currentNode = new graphNode(laneIdx);
        allNodes.push(currentNode);

        if (prevNode) prevNode.forward.setToNode(currentNode);

        // Add more edges if node is a switch

        // TODO: Check that index 0 is actually the leftmost
        if (laneIdx > 0) {
          // Can switch left
          currentNode.left = new graphEdge(0, null);
          currentNode.left.isLeft = true;
          currentNode.edges.push(currentNode.left);

        }
        if (laneIdx < lanes.length-1) {
          // Can switch right

          currentNode.right = new graphEdge(0, null);
          currentNode.right.isRight = true;
          currentNode.edges.push(currentNode.right);

        }

        // Set current lane node
        laneNodes[laneIdx] = currentNode;
        // Set prev lane node
        prevLaneNodes[laneIdx] = prevNode;

      }


      // Create nodes for switch ends or track end
      if (wasSwitch || i === pieces.length-1) {
        // Create new node and set prevNodes edge

        prevNode = laneNodes[laneIdx];
        currentNode = new graphNode(laneIdx);
        allNodes.push(currentNode);

        prevNode.forward.setToNode(currentNode);

        // Set current lane node
        laneNodes[laneIdx] = currentNode;
        // Set prev lane node
        prevLaneNodes[laneIdx] = prevNode;


      }

    }


    // Update edges after laneArrays have been updated
    if (wasSwitch) {

      for (laneIdx = 0; laneIdx < lanes.length; ++laneIdx) {

        currentNode = laneNodes[laneIdx];
        prevNode = prevLaneNodes[laneIdx];
          
        if (laneIdx > 0) {
          // Was able to switch right
          prevLaneNodes[laneIdx-1].right.setToNode(currentNode);

        }
        if (laneIdx < lanes.length-1) {
          // Was able to switch left
          prevLaneNodes[laneIdx+1].left.setToNode(currentNode);

        }

      }

    }


    // Reset switch check
    wasSwitch = isSwitch;
    isSwitch = false;

  }

  // console.log('pieces.length', pieces.length);
  // console.log('nodesTotal', allNodes.length);

};

var loopGraph = function() {

  startNodes.forEach(function(startNode) {

    var currentNode = startNode;
    var totalWeight = 0;

    while (currentNode.forward) {

      totalWeight += currentNode.forward.weight;
      currentNode = currentNode.forward.toNode;

    }

    // console.log('Lane ' + startNode.laneIndex + ': ' + totalWeight);
  });

};

// Pseudo
// let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)
// let next be a |V| × |V| array of vertex indices initialized to null

var nodesLen, dist, next;

var initArrays = function() {

  nodesLen = allNodes.length;
  dist = new Array(nodesLen);
  next = new Array(nodesLen);

  for (var i = 0; i < nodesLen; ++i) {
    dist[i] = new Array(nodesLen);
    next[i] = new Array(nodesLen);
    for (var j = 0; j < nodesLen; ++j) {
      dist[i][j] = Infinity;
      next[i][j] = null;
    }
  }

};

// procedure initNextArray ()
//    for i from 1 to |V|
//       for j from 1 to |V|
//          if i = j or dist[i][j] = ∞ (infinity) then
//             next[i][j] ← 0
//          else
//             next[i][j] ← i
var initNextArray = function() {
  for (var i = 0; i < nodesLen; ++i) {
    for (var j = 0; j < nodesLen; ++j) {
      if (i == j || dist[i][j] == Infinity) {
        next[i][j] = null;
      } else {
        next[i][j] = i;
      }
    }
  }
};

// procedure FloydWarshallWithPathReconstruction ()
//    for each vertex v
//       dist[v][v] ← 0
//    for each edge (u,v)
//       dist[u][v] ← w(u,v)  // the weight of the edge (u,v)
//    initNextArray()
//    for k from 1 to |V|
//       for i from 1 to |V|
//          for j from 1 to |V|
//             if dist[i][k] + dist[k][j] < dist[i][j] then
//                dist[i][j] ← dist[i][k] + dist[k][j]
//                next[i][j] ← k
var FloydWarshallWithPathReconstruction = function() {

  initArrays();

  for (var i = 0, n; i < nodesLen; ++i) {

    n = allNodes[i];
    dist[i][i] = 0;

    for (var j = 0, edge, k; j < n.edges.length; ++j) {
      edge = n.edges[j];

      k = allNodes.indexOf(edge.toNode);

      dist[i][k] = edge.weight;
    }

  }

  initNextArray();

  for (var k = 0; k < nodesLen; ++k) {
    for (var i = 0; i < nodesLen; ++i) {
      for (var j = 0; j < nodesLen; ++j) {
        if (dist[i][k] + dist[k][j] < dist[i][j]) {
           dist[i][j] = dist[i][k] + dist[k][j];
           next[i][j] = k;
        }
      }
    }
  }

};


// function Path (i, j)
//    if dist[i][j] = ∞ then
//      return "no path"
//    var intermediate ← next[i][j]
//    if intermediate = i then
//      return " "   // the direct edge from i to j gives the shortest path
//    else
//      return Path(i, intermediate) + intermediate + Path(intermediate, j)

var Path = function(i, j) {
  if (dist[i][j] === Infinity) {
    return "no path";
  }
  var intermediate = next[i][j];
  if (intermediate === i) {
    return " ";
  } else {
    return Path(i, intermediate) + intermediate + Path(intermediate, j);
  }
};


var findShortest = function(lane) {

  var shortest = Path(lane,31);

  // console.log('shortest path from 0 to ' + (nodesLen-1) + ': ' + shortest);

  var res = [];

  var idxs = shortest.trim().split(" ");

  // console.log(idxs);

  for (var j = 0, i, lastLaneIdx, laneIdx; j < idxs.length; ++j) {
    i = parseInt(idxs[j], 10);

    if (!i) continue;

    laneIdx = allNodes[i].laneIndex;

    if (j % 2 !== 0) {

      if (laneIdx == lastLaneIdx) {
        // console.log('straight', lastLaneIdx, laneIdx);
        res.push('Straight');
      } else if (laneIdx > lastLaneIdx) {
        // console.log('right', lastLaneIdx, laneIdx);
        res.push('Right');
      } else {
        // console.log('left', lastLaneIdx, laneIdx);
        res.push('Left');
      }

    }

    lastLaneIdx = laneIdx;

  }

  return res;


};

// Test

buildGraph();
// loopGraph();
FloydWarshallWithPathReconstruction();
// findShortest();

exports.findShortest = findShortest;