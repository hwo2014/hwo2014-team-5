/* Coder Coded - HWO BOT RACER - Authors: Juha Rantanen, Joonas Ruuskanen, Pekka Toiminen */

var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2] || 'hakkinen.helloworldopen.com';
var serverPort = process.argv[3] || 8091;
var botName = process.argv[4] || 'MyMom';
var botKey = process.argv[5] || 'TSqmXvBn09Z+Hw';

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

// a script that's "out of our reach" checks for args etc. Don't touch above.

// =========================================================================
// CODE STARTS HERE
// =========================================================================

// For plotting data
var fs = require('fs');

var botColor;
var gameId;
var gameTick;
var tickCount = 0;
var firstThrottle = true;

var trackData;

var trackId;
var trackName;

var laps;
var maxLapTimeMs;
var quickRace;

var pieceIndex;
var inPieceDistance;
var lap;

var numLanes = 0;
var lanes = [];
var iLanes = [];
var pieces = [];
var switchPieceIndexes = [];
var laneLengths = [];
var lapModulo = 0;
var switchPieceModulo = 0;

// tick variables
var leftLanes = [];
var rightLanes = [];
var currentLane = 0;

var carAngle = 0;
var pieceAngle = 0;
var inSwitch = false;

var otherCars = {};
var myCar = {};

// flag to tell whether a switch has been sent for next switch piece
// only two lane switches can be cancelled when a new switch is sent
// to server. Otherwise new switch can also be issued...
var noSwitches = true;

//var PI = Math.PI;
//var PI = 3.14159;
var PI = 3.142;
var DEGRA = PI/180;


// parameters that we need to figure out
// Distance:
var p_pieceIndex = -1;
var p_inPieceDistance = -1;
var p_currentLane = -1;

var friction = 0;
var power = 0;

// I want to be able to predict car angle, speed and acceleration on every part of lane.
// plot a curve of throttle, ticks, speed etc.

var velocity = 0;
var p_velocity = 0;
var acceleration = 0;
var p_acceleration = 0;
var jerk = 0;

var aVelocity = 0;
var p_aVelocity = 0;
var aAcceleration = 0;
var p_aAcceleration = 0;
var aJerk = 0;

// Array used for plotting
var resultData = [];
// Stop collecting data after crash (for now)
var crashDetected = false;

// var currentSpeed = 0;
// var currentAcceleration = 0;
// var maxSpeed = 0;
// var maxAcceleration = 0;

// used for advanced testing
var trackName = 'keimola';
var trackName = 'germany';
var password = '';
var carCount = 1;

// Select race type -- NOTE: USE 'join' FOR AUTOMATED TEST RUNS!

var dataDict = { name: botName, key: botKey };
var raceDict = { msgType: 'join', data: dataDict };
//var raceDict = { msgType: 'joinRace', data: {botId: dataDict, trackName: trackName, password: password, carCount: carCount }};
//var raceDict = { msgType: 'createRace', data: {botId: dataDict, trackName: trackName, password: password, carCount: carCount }};
//var raceDict = { msgType: 'joinRace', data: {botId: dataDict, trackName: trackName, carCount: carCount}};

// Connect to server

client = net.connect( serverPort, serverHost, function() { return send( raceDict ) });
jsonStream = client.pipe(JSONStream.parse());

// Game loop

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    gameId = data.gameId;
    gameTick = data.gameTick;

    updateCarPositions(data.data)

    ctrl = naiveAI(data.data);


    if (!crashDetected) {
      resultData.push({
        'tick': gameTick,
        'throttle': ctrl.throttle,
        'velocity': velocity,
        'acceleration': acceleration,
        'jerk': jerk,
        'pieceAngle': pieceAngle,
        'carAngle': carAngle,
        'aVelocity': aVelocity,
        'aAcceleration': aAcceleration,
        'aJerk': aJerk
      });
      
      // resultData.push({tick: tickCount, variable: 'throttle', value: ctrl.throttle});
      // resultData.push({tick: tickCount, variable: 'velocity', value: velocity });
      // resultData.push({tick: tickCount, variable: 'acceleration', value: acceleration });
      // resultData.push({tick: tickCount, variable: 'jerk', value: jerk });
      // resultData.push({tick: tickCount, variable: 'carAngle', value: carAngle });
      // resultData.push({tick: tickCount, variable: 'aVelocity', value: aVelocity });
      // resultData.push({tick: tickCount, variable: 'aAcceleration', value: aAcceleration });
      // resultData.push({tick: tickCount, variable: 'aJerk', value: aJerk });

    }

    // console.log('%s, t: %s, v: %s, a: %s', gameTick, ctrl.throttle, velocity, acceleration);
    
    tickCount += 1;

    if (ctrl.switchLane) {
      console.log('Sent SwitchLane!');
      send({
        msgType: "switchLane",
        data: ctrl.targetLane,
        gameTick: gameTick
      })
    } else {
      send({
        msgType: "throttle",
        data: ctrl.throttle,
        gameTick: gameTick
      });
    }
  
  } else {
   
    if ( data.msgType === 'yourCar' ) {

      botColor = data.data.color;
      console.log('This bot color: ' + botColor);

    } else if ( data.msgType === 'gameInit' ) {
  
      firstThrottle = true;
      //console.log(JSON.stringify(data));
      trackData = data.data.race;

      trackId = trackData.track.id;
      trackName = trackData.track.name;

      console.log( 'Racing on: ' + trackId + ' name: ' + trackName );

      laps = trackData.raceSession.laps;
      maxLapTimeMs = trackData.raceSession.maxLapTimeMs;
      quickRace = trackData.raceSession.quickRace;

      preprocessRoutes(trackData);
      //processCars(trackData)

    } else if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
      firstThrottle = true;

    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'crash') {
      crashDetected = true;
      console.log(JSON.stringify(data));
    } else {
      console.log(JSON.stringify(data));
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected due to stream error - not our code");
});


function updateCarPositions( positions ) {
  // Updates:
  // myCar
  // otherCars
  // carAngle
  // pieceAngle
  // inSwitch
  // currentLane
  // leftLanes
  // rightLanes
  // pieceIndex
  // inPieceDistance
  // lap

  p_pieceIndex = pieceIndex;
  p_inPieceDistance = inPieceDistance;
  p_currentLane = currentLane;
  p_carAngle = carAngle;

  for ( var i = 0; i < positions.length; i++ ) {
    if ( positions[i].id.color == botColor ) {
      myCar = positions.pop(i);
      otherCars = positions;
      break;
    }
  }

  carAngle = myCar.angle;
  
  // Calculate lanes to left & right, and whether we're in switch atm.
  inSwitch = false;
  currentLane = myCar.piecePosition.lane.startLaneIndex;
  if (currentLane != myCar.piecePosition.lane.endLaneIndex) {
    inSwitch = true;
  }

  leftLanes = iLanes.slice(0,iLanes.indexOf(currentLane));
  rightLanes = iLanes.slice(iLanes.indexOf(currentLane)+1);

  pieceIndex = myCar.piecePosition.pieceIndex;
  inPieceDistance = myCar.piecePosition.inPieceDistance;

  lap = myCar.piecePosition.lap;

  pieceAngle = pieces[pieceIndex].hasOwnProperty('angle') ? pieces[pieceIndex].angle : 0;

  // these can be optimized for race (as qualify is the same track as race)
  if (tickCount > 0) {
    p_velocity = velocity;
    velocity = calcVelocity();
    aVelocity = carAngle - p_carAngle;
  }

  if (tickCount > 1) {
    p_acceleration = acceleration;
    acceleration = velocity - p_velocity;

    p_aAcceleration = aAcceleration;
    aAcceleration = aVelocity - p_aVelocity;
  }

  if (tickCount > 2) {
    jerk = acceleration - p_acceleration;
    aJerk = aAcceleration - p_aAcceleration;
  }

  // console.log ('current v: %s, a: %s, jerk: %s, angle: %s, av: %s, aa: %s, aj: %s', velocity, acceleration, jerk, carAngle, aVelocity, aAcceleration, aJerk);

}

function calcVelocity() {
  // calculates traveled distance / tick
  // problematic during switches...

  if (p_pieceIndex == pieceIndex) {
    return inPieceDistance - p_inPieceDistance;
  } else {
    return (laneLengths[p_pieceIndex][p_currentLane].keep - p_inPieceDistance) + inPieceDistance;
  }

}

function preprocessRoutes( trackData ) {
  // Calculates:
  // lanes  (index, distanceFromCenter)
  // iLanes (just the indexes)
  // numLanes
  // pieces
  // lapModulo
  // switchPieceIndexes
  // laneLengths
  // switchPieceModulo

  //console.log(JSON.stringify(trackData));

  // WE MUST ALSO TAKE INTO ACCOUNT THE LANE WIDTHS AT SOME POINT!


  // ensure that our lanes are sorted from left to right
  lanes = trackData.track.lanes;
  lanes.sort( function(a, b) { return a.distanceFromCenter - b.distanceFromCenter });
  numLanes = lanes.length;

  for (var i = 0; i < lanes.length; i++) {
    iLanes.push(lanes[i].index);    
  }

  pieces = trackData.track.pieces;
  lapModulo = pieces.length;

  for ( var i = 0; i < pieces.length; i++ ) {
    if ( pieces[i].hasOwnProperty('switch') ) {
      if ( pieces[i].switch ) {
        switchPieceIndexes.push(i);  
      }
    }
    laneLengths.push( calculateLaneLengths( i ) );
  }

  swicthPieceModulo = switchPieceIndexes.length;

  console.log('--| Preprocess Routes |--');
  console.log('numLanes: ' + numLanes);
  console.log('lanes: ' + JSON.stringify(lanes));
  console.log('lapModulo: ' + lapModulo);
  console.log('switchPieceIndexes: ' + switchPieceIndexes);
  console.log('switchPieceModulo: ' + switchPieceModulo);
  console.log(JSON.stringify(laneLengths));

}

function getNextSwitchIndex( fromPieceIndex ) {
  // returns index to switchPieceIndexes list
  // it points to next switch piece from given piece index

  var i = fromPieceIndex;
  var nextSwitchIndex = switchPieceIndexes.indexOf(i);
  while (nextSwitchIndex < 0) {
    i = (i + 1) % lapModulo;
    nextSwitchIndex = switchPieceIndexes.indexOf(i);
  }
  return nextSwitchIndex;
}

function getKeepDistance( targetIndex ) {

  // no switches in between, just distance to targetIndex
  // when current lane is kept. We return negative distances
  // if we're already driving on the targetIndex

  var i = pieceIndex;
  var distanceToPiece = 0;

  if (i != targetIndex) {
    distanceToPiece += laneLengths[i][currentLane].keep - inPieceDistance;
    while ( i != targetIndex - 1) {
      distanceToPiece += laneLengths[i][currentLane].keep;
      i = (i + 1) % lapModulo;
    }
  } else {
    distanceToPiece -= inPieceDistance;
  }

  return distanceToPiece;
}


function getKeepLength( startIndex, endIndex, lane ) {
  // calculates length from startIndex (including it)
  // to endIndex (excluding it)

  var i = startIndex;
  var keepLength = 0;

  while ( i != endIndex) {
    keepLength = keepLength + laneLengths[i][lane].keep;
    i = (i + 1) % lapModulo;
  }

  return keepLength;
}

function getSwitchLengths( startSwitch, endSwitch ) {

  // dir: 0 = keep, -1 = left, +1 = right

  var r = []
  
  r.push({
    dir: 0,
    len: getKeepLength( startSwitch, endSwitch, currentLane )
  });
  
  if (leftLanes.length > 0) {
    r.push({
      dir: -1,
      len: getKeepLength( startSwitch+1, endSwitch, leftLanes[leftLanes.length-1] ) + laneLengths[startSwitch][currentLane].left 
    });
  }

  if (rightLanes.length > 0) {
    r.push({
      dir: 1,
      len: getKeepLength( startSwitch+1, endSwitch, rightLanes[0] ) + laneLengths[startSwitch][currentLane].right
    });
  }

  return r;  
}


function projectPieceProbe( distance ) {
  // here I practice some probing...
  // this function returns the piece information that the
  // probe is in, and the angle if it is a curved piece
  // pieceIndex is our current piece
  // inPieceDistance is our traveled distance on that piece
  // check first how many pieces we must travel
  var d = distance;
  var p = pieceIndex;

  // first take off this piece
  d -= laneLengths[p][currentLane].keep - inPieceDistance;
  if (d <= 0) {
    // we are on the current piece
    return laneLengths[p][currentLane];
  }
  while (d > 0) {
    p = (p + 1) % lapModulo;
    d -= laneLengths[p][currentLane].keep;
  }
  return laneLengths[p][currentLane];
}

// function projectProbe( distance ) {
//   // this function returns the probe vector from our current location
//   // it returns the length of the tangential component, length of the
//   // normal component and the angle between those two.
//   var d = distance;
//   var p = pieceIndex;
  
//   var dy = 0;
//   var dx = 0;
//   var theta = 0;

//   while (d > 0) {
//     if (p == pieceIndex) {
//       d -= (laneLengths[p][currentLane].keep - inPieceDistance)
//     }
//   }

function routeAdvisor() {

  // Task 1: Give always shortest route, distancewise.
  // pieceIndex, inPieceDistance, switchPieceIndexes

  var nextSwitchIndex = getNextSwitchIndex( pieceIndex );
  var nextSwitchPiece = switchPieceIndexes[ nextSwitchIndex ];
  
  var nextSwitchIndex2 = getNextSwitchIndex( (nextSwitchPiece + 1) % lapModulo );
  var nextSwitchPiece2 = switchPieceIndexes[ nextSwitchIndex2 ];

  var distanceToSwitchPiece = getKeepDistance( nextSwitchPiece );

  var shortestLane = getSwitchLengths( nextSwitchPiece, nextSwitchPiece2 );
  shortestLane.sort( function(a, b) { return a.len - b.len });

  var pieceProbe = projectPieceProbe( velocity * 10 );

  // now we have distanceToSwitchPiece and shortestLane...

  // console.log(switchPieceIndexes);
  // console.log(shortestLane);
  // console.log('Next Switch Index: ' + nextSwitchPiece);
  // console.log('Distance To Switch Piece: ' + distanceToSwitchPiece);



  // i = pieceIndex;
  // var distanceToSwitchPiece = 0;

  // if (i != nextSwitchIndex) {
  //   distanceToSwitchPiece += laneLengths[pieceIndex][currentLane].keep - inPieceDistance;
  //   while (i != nextSwitchPiece) {
  //     i = (i + 1) % lapModulo;
  //     distanceToSwitchPiece += laneLengths[pieceIndex][currentLane].keep;
  //   }
  // } else {
  //   distanceToSwitchPiece -= inPieceDistance;
  // }


  // calculate suggested route from next switch (naive shortest one)
  // we can only switch from [left + right] on this lane

  return {
    nextSwitchIndex: nextSwitchIndex,
    distanceToSwitchPiece: distanceToSwitchPiece,
    shortestLane: shortestLane,
    pieceProbe: pieceProbe
  };

}

function calculateLaneLengths( pieceIndex ) {
  // returns all lane lengths of given pieceIndex
  var r = [];
  var p = pieces[pieceIndex];

  var c = 0, cl = 0, cr = 0; // Curve length
  var sll = 0;
  var slr = 0;
  var ldl = 0, ldr = 0; // Lane distance

  // straight piece with or without switch
  if ( p.hasOwnProperty('length') ) {
    for ( var i = 0; i < numLanes; i++ ) {
      if ( p.hasOwnProperty('switch') ) {   

        if (lanes[i-1]) {
          ldl = Math.abs(lanes[i-1].distanceFromCenter - lanes[i].distanceFromCenter);
          sll = Math.sqrt(Math.pow(ldl, 2) + Math.pow(p.length, 2));
        }

        if (lanes[i+1]) {
          ldr = Math.abs(lanes[i+1].distanceFromCenter - lanes[i].distanceFromCenter);
          slr = Math.sqrt(Math.pow(ldr, 2) + Math.pow(p.length, 2));
        }

        if (i == 0) {
          r.push({ angle: 0, switchPiece: true, keep: p.length, right: slr });
        } else if (i == (numLanes - 1)) {
          r.push({ angle: 0, switchPiece: true, keep: p.length, left: sll });
        } else {
          r.push({ angle: 0, switchPiece: true, keep: p.length, left: sll, right: slr });
        }
      } else {
        r.push({ angle: 0, switchPiece: false, keep: p.length });
      }   
    }
  } else {
    // curved piece with or without switch
    var radius = 0, rl = 0, rr = 0;
    for ( var i = 0; i < numLanes; i++ ) {

      if (p.angle > 0) {
        radius = p.radius - lanes[i].distanceFromCenter;

      } else {
        radius = p.radius + lanes[i].distanceFromCenter;  
      }
      c = (Math.abs(p.angle)*DEGRA) * radius;

      if ( p.hasOwnProperty('switch') ) {

        if (lanes[i-1]) {
          ldl = Math.abs(lanes[i-1].distanceFromCenter - lanes[i].distanceFromCenter);
        }

        if (lanes[i+1]) {
          ldr = Math.abs(lanes[i+1].distanceFromCenter - lanes[i].distanceFromCenter);
        }

        if (p.angle > 0) {
          rl = radius - ldl;
          rr = radius - ldr;

        } else {
          rl = radius + ldl;
          rr = radius + ldr;
        }

        cl = (Math.abs(p.angle)*DEGRA) * rl;
        cr = (Math.abs(p.angle)*DEGRA) * rr;
        
        sll = (c + cl) / 2;
        slr = (c + cr) / 2;

        if (i == 0) {
          r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, right: slr });
        } else if (i == (numLanes - 1)) {
          r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, left: sll, right: slr });
        } else {
          r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, left: sll, right: slr });
        }
      } else {
        r.push({ angle: p.angle, radius: radius, switchPiece: false, keep: c });
      }
    }
  }
  return r;
}

function naiveAI(data) {
  
  var ctrl = {
    switchLane: false,
    targetLane: 'Left',
    throttle: 0.5
  }

  // return ctrl;

  // GameStarts - throttle 1.0 for 2 ticks to get our
  // measurements running full power.

  if (firstThrottle) {
    if (acceleration == 0) {
      return ctrl;    
    } else {
      firstThrottle = false;
    }
  }

  // ask from routeAdvisor what's the best switch
  // from next switchpiece. determine based on response
  // whether to issue it, and when.
  advisory = routeAdvisor();

  //nextSwitchIndex: nextSwitchIndex,
  //distanceToSwitchPiece: distanceToSwitchPiece,
  //shortestLane: shortestLane (keep, left, right)

  if (advisory.distanceToSwitchPiece < 100 && advisory.distanceToSwitchPiece > 0 && noSwitches) {
    var dir = advisory.shortestLane[0].dir;
    console.log('advisory.shortestLane', advisory.shortestLane, dir);
    noSwitches = false;
    if (dir != 0) {
      ctrl.targetLane = 'Right';
      ctrl.switchLane = true;
      if (dir < 0) {
        ctrl.targetLane = 'Left';
      }
    }

  }
  if (advisory.distanceToSwitchPiece > 100) {
    noSwitches = true;
  }

  ctrl.throttle = 1.0;

  var absAngle = Math.abs(advisory.pieceProbe.angle)
  if (absAngle > 0) {
    var force = velocity*velocity / advisory.pieceProbe.radius;
    console.log('force: ', force);
    ctrl.throttle = force;
    if (force < 0.3) {
      if (velocity > 6.0) {
        ctrl.throttle = 0.0;
      } else {
        ctrl.throttle = 1.0;
      }
    }
  }


  //console.log('%s, %s, %s', velocity, carAngle, advisory.pieceProbe.angle);

  // if (pieceIndex > )

  // if (velocity > 6.5) {
  //   ctrl.throttle = 0.1;
  // }

  // if (pieceIndex > 34 ) {
  //   ctrl.throttle = 1.0;
  // }

  // if (pieceIndex > 7 && pieceIndex < 13) {
  //   ctrl.throttle = 1.0;
  // }

  // if (pieceIndex < 3) {
  //   ctrl.throttle = 1.0;
  // }
  //console.log(advisory.distanceToSwitchPiece);

  // var ourPosition;

  // for ( var i = 0; i < data.length; i++ ) {
  //   if ( data[i].id.color == carColor ) {
  //     ourPosition = data[i];
  //   }
  
    // we can also track other cars here
  //}

  // we have:
  // ourPosition.angle
  // ourPosition.piecePosition
  //                          .pieceIndex
  //                          .inPieceDistance
  //                          .lap
  //                          .lane
  //                               .startLaneIndex
  //                               .endLaneIndex
  //
  // check that we are on the innermost lane...
  // lanes are described in raceTrack.lanes
  // we must check from track description what is the closest to center!

  // var rights = [2, 16]


  // if (rights.indexOf(pieceIndex) > -1 && switchable && !inSwitch) {
  //   ctrl.targetLane = 'Right';
  //   ctrl.switchLane = true;
  //   switchable = false;
  //   console.log(pieceIndex);
  //   console.log(inPieceDistance);
  // }

  // var lefts = [7]

  // if (lefts.indexOf(pieceIndex) > -1 && switchable && !inSwitch) {
  //   ctrl.targetLane = 'Left';
  //   ctrl.switchLane = true;
  //   switchable = false;
  //   console.log(pieceIndex);
  //   console.log(inPieceDistance); 
  // }

  // if (!inSwitch) {
  //   switchable = true;
  // }

  // if (lap > 1 && pieceIndex >= 31) {
  //   foobool = 0;
  // }
  //   ctrl.throttle = 1.0;
  //   //console.log('successful switch!');
  // } else {
  //   switchable = true;
  //   if (foobool) {
  //     ctrl.throttle = 0.5;
  //     foobool = false;
  //   } else {
  //    ctrl.throttle = 1.0;
  //    foobool = true;
  //   }
  //   //ctrl.throttle = 0.5;
  // }

  // if (switchable) {
  //   ctrl.targetLane = 'Right';
  //   ctrl.switchLane = true;
  //   switchable = false;
  // }

  // just check that when we are close to curves, drop throttle to
  // 0.6 and on straigths put it to 1.0
  //console.log(ourPosition.angle);

  // if (foobool == 0) {
  //   ctrl.throttle = 1.0;
  //   foobool += 1;
  // } else if ( foobool == 1 ) {
  //   ctrl.throttle = 0.2;
  //   foobool += 1;
  // } else {
  //   ctrl.throttle = 0.8;
  //   foobool = 0;
  // }

  return ctrl;


}

// Export some data before exiting
process.on( 'SIGINT', function() {

  var dataJson = JSON.stringify(resultData, null, 2);
  var fileName = "data.json";

  fs.writeFile("./" + fileName, dataJson, function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("Collected data exported to " + fileName);
    }
    console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
    process.exit();
  });

});
