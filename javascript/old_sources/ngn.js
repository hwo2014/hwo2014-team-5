// HWO physics engine
var Vec2 = require('./Vec2.js').Vec2;

var Ngn = function() {

  // TODO:
  // REDO THIS SHIT!! :D :D :D
  // SIMPLIFY MODELS:
  // FORGET ABOUT DRAG
  // ONLY FRICTION AND ENGINE POWER
  // ENGINE POWER IS An inverse function of d*v^2.. smaller v, larger power
  // probably only 1 friction in use.


  // SET CAR ANGLE
  // most likely sin(theta) * drag/friction = rotational force
  // most likely cos(theta) * drag/friction = slowing down force
  // tee testit suoralla tiellä!! (vai onko niin että auto hidastuu ihan täpöllä,
  // vai meneekö osa energiasta pyörimiseen ja hidastuminen on hitaampaa?????)

  // WE HAVE PRETTY GOOD MODEL (AT LEAST WHAT COMES TO STRAIGHTS.. )
  // WE NEED TO COME UP WITH A WAY TO PREDICT THE VALUES FOR THIS MODEL FROM GAME DATA!
  // location vector or distance traveled...
  // rotational force is affected by friction when no other rotation applied

  // car properties (width, length, guide flag position [=origin], density, drag coefficient)
  this.w = 2.0;
  this.l = 4.0;
  this.gf = 0.2;
  this.gb = this.l - this.gf;
  this.rho = 1.15;
  this.D = this.w*1.2922*0.5*15.11E-5;
  console.log('D is now: %s', this.D);
  
  // car vectors (velocity, acceleration, angle, angular velocity, angular acceleration )
  this.vel = new Vec2(0, 0);
  this.acc = new Vec2(0, 0);
  this.ang = new Vec2(1, 0);
  this.aVel = 0;
  this.aAcc = 0;

  // constants (mass, gravity, rolling friction coeff, static friction coeff, sliding friction coeff)
  this.m = this.rho * this.w * this.l;
  this.g = 9.81;
  this.mu_r = 0.005;
  this.mu_s = 0.04;
  this.mu_l = 0.02;

  // forces (tan, static friction, rolling friction, sliding friction, drag force, net force)
  this.tF = new Vec2(0, 0);
  this.sF = new Vec2(this.mu_s * this.m * this.g, 0);
  this.rF = new Vec2(this.mu_r * this.m * this.g, 0);
  this.lF = new Vec2(this.mu_l * this.m * this.g, 0);
  this.dF = new Vec2(0, 0);
  this.nF = new Vec2(0, 0);

  // track vector (unit vector giving the direction of track)
  this.track = new Vec2(1, 0);

  // root coordinate vectors
  this.unit = new Vec2(1, 0);

  // power and throttle functions (maximum throttle, maximum power, terminal velocity)
  this.maxT = 1.0;
  this.maxP = 1.0;
  this.fP = function() { return this.maxP };
  this.fT = function(t) { return 0.5 };
  this.vt = Math.sqrt((this.m * this.maxT * this.maxP) / this.D);

  // time
  this.step = 0;
}

Ngn.prototype.reset = function() {

  this.vel.set( 0, 0 );
  this.acc.set( 0, 0 );
  this.ang.set( 1, 0 );

  this.tF.set( 0, 0 );
  this.sF.set( this.mu_s * this.m * this.g, 0 );
  this.rF.set( this.mu_r * this.m * this.g, 0 );
  this.lF.set( this.mu_l * this.m * this.g, 0 );
  this.dF.set( 0, 0 );
  this.nF.set( 0, 0 );

  this.track.set( 1, 0 );

  this.step = 0;

}

Ngn.prototype.advance = function( n ) {
  // advance engine n steps
  for (var i = 0; i < n; i++) {
    this.cout();
    this.trackAngle( 0 );
    this.tanForce(i);
    this.dragForce();
    this.netForce();
    this.updateCar();
    this.step++;
  }
}

Ngn.prototype.cout = function() {
  //console.log('step: %s, thrt: %s, angle: %s, nF: %s, vel: %s, acc: %s', this.step, this.fT(), this.ang.r2d(this.ang.angle(this.unit)), this.nF.mag(this.track), this.vel.len(), this.acc.mag(this.track) );
  //console.log('terminal velocity: %s', Math.sqrt((this.m * this.maxT * this.maxP)/this.D));
  console.log('step: %s, vt: %s, throttle: %s, vel: %s, acc: %s, nf: %s', this.step, this.vt, this.fT(), this.vel.mag(this.track), this.acc.mag(this.track), this.nF.mag(this.track));
}

Ngn.prototype.trackAngle = function( a ) {
  // this.unit is in 0 angle.
  // track is always the track tangent direction
  var p = this.track.r2d( this.track.angle( this.unit ) );
  var b = this.track.d2r( a - p );
  this.track.rotate( b );
  this.sF.rotate( b );
  this.rF.rotate( b );
}

Ngn.prototype.tanForce = function( t ) {
  // Comes from engine (throttle & power)
  this.track.copyTo( this.tF );
  this.tF.makeLen( this.m * this.fT( t ) * this.fP() );
}

Ngn.prototype.dragForce = function() {
  // Comes from air resistance, related to speed & shape.
  this.track.copyTo( this.dF );
  this.dF.makeLen( this.D * this.vel.len() * this.vel.len() );
}

Ngn.prototype.netForce = function () {
  
  if (this.vel.mag(this.track) < 0) this.vel.set(0, 0);

  // tangentForce less than static friction - can not move.
  if (this.vel.len() == 0 && this.tF.len() < this.sF.len()) {
    this.nF.set(0, 0);
  } else {
    this.tF.copyTo( this.nF );
    this.nF.subSelf( this.dF );
    this.nF.subSelf( this.rF );
  }
}

Ngn.prototype.updateCar = function() {
  
  // drive
  this.nF.copyTo( this.acc );
  this.acc.scale( 1 / this.m );
  this.vel.addSelf( this.acc );
  if (this.vel.mag(this.track) < 0) {
    this.vel.set(0, 0);
  }

  // torque
  // aVel is affected by aAcc.
  // aAcc is caused by lF - sliding friction WHEN angle between track & car > 0.
  // it also operates to different directions at the end of (gf and gb).
  // torque depends on R then.
  // var angle = this.ang.r2d(this.ang.angle(this.unit));
  // var torque1 = Math.sin(angle) * (this.mu_l * this.gf);
  // var torque2 = Math.sin(angle) * (this.mu_l * this.gb);
  // var torque = torque2 - torque1;
  // this.aVel += torque;
  // console.log(this.aVel);
  // this.ang.rotate(this.aVel);


  //console.log('torque1: %s , torque2: %s, torque: %s, angle: %s', torque1, torque2, torque, angle);
  // if (this.ang != 0) {
  //   // calculate total sum to angular acceleration using R torque...
  // }
  // this.track.copyTo( this.aAcc );
  // this.aAcc.orto();
  // this.aAcc.scale( 1 / this.m );
  // this.aVel.addSelf( this.aAcc );
  // this.ang.rotate(this.aVel.len())
  // console.log(this.aVel);

}

// Ngn.prototype.tanForce = function( t, p ) {
//   this.Ftan = this.m * t * p( this.v ); 
// }



var ngn = new Ngn();
//ngn.ang.rotate(ngn.ang.d2r(2));
ngn.advance(10);
//ngn.fT = function() { return 0.0; }
//ngn.advance(10);

exports.Ngn = Ngn;