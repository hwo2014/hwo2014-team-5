// Throttle Controller

var tC = function( precision, debug_msg ) {
	
	// decimal precision for calculations
	this.precision = precision;
	
	this.debug_msg = debug_msg;

	// estimated from gathered throttle data
	this.magic = -0.02020282562771;

	//  acceleration base curve (not needed)
	var j = 0;
	this.accCurve = [];
	for ( var i = 0; i < 30000; i++) {
	  this.accCurve.push( Math.pow( Math.E, ( this.magic * j )));
		j += 0.1;
	}

	// velocity base curve
	this.velCurveLen = 30000;
	this.velCurve = [1.0];
	for ( var i = 1; i < this.velCurveLen; i ++) {
	  this.velCurve.push((this.velCurve[i-1] + Math.pow( Math.E, ( this.magic * i ))));
	}

	this.braCurve = [this.velCurve[this.velCurveLen-1]];
	for ( var i = 1; i < this.velCurveLen; i ++) {
		this.braCurve.push((this.braCurve[i-1] - Math.pow( Math.E, ( this.magic * i ))));
	}



	// power and throttle coefficients
	this.power = 10.0;
	this.throttle = 0.5;
	this.brakeThrottle = 0;
	this.curveCoeff = function(t) { return t * 2.0 / this.power };
	this.aCurve = function(i, t) { return this.accCurve[i]*this.curveCoeff(t) };
	this.vCurve = function(i, t) { return this.velCurve[i]*this.curveCoeff(t) };
	this.bCurve = function(i, t) { return this.braCurve[i]*this.curveCoeff(t) };
}

tC.prototype.setThrottle = function( t, v ) {
	this.throttle = t;
	this.changeVel = v;
	this.maxVel = t * this.power;
	if ( this.vCurve( this.velCurveLen - 1, t ) < v ) {
		this.brakeThrottle = 1.0*(v - t*this.power.toFixed(2));
		this.brakeThrottle = v;
	} else {
		this.brakeThrottle = 0;
	}
	return t;
}

tC.prototype.dbg_vCurve = function( start, end, t ) {
	for ( var i = start; i < end; i++ ) {
		console.log('vCurve(%s, %s): %s', i, t, this.vCurve( i , t ));
	}
}

tC.prototype.dbg_bCurve = function( start, end, t ) {
	for ( var i = start; i < end; i++ ) {
		console.log('bCurve(%s, %s): %s', i, t, this.bCurve( i , t ));
	}
}

tC.prototype.ticks2vel = function( startVelocity, t, ticks ) {
	var i = 0;
	if (this.brakeThrottle) {
		// we use brakeThrottle as our base curve and add
		// current throttle to its base value. We apply this
		while ( this.bCurve(i, 1.0 - t) > startVelocity) {
			i++;
		}
		console.log('brakecurve i matched: %s', i);
		return -1*this.bCurve(i + ticks, 1.0 - t).toFixed( this.precision );
	} else {
		while ( this.vCurve(i, t) < startVelocity ) {
			i++;
		}
		console.log('i matched: %s', i);
		if (Math.abs(this.vCurve(i, t) - startVelocity) > Math.abs(startVelocity - this.vCurve(i-1, t))) {
			i = i - 1;
		}
		return this.vCurve(i + ticks, t).toFixed( this.precision );
	}
}

tC.prototype.ticks2acc = function( startAcceleration, t, ticks ) {
	var i = 0;
	while ( this.aCurve(i, t) > startAcceleration ) {
		i++;
	}
	if (Math.abs(this.aCurve(i, t) - startAcceleration) > Math.abs(startAcceleration - this.aCurve(i-1, t))) {
		i = i - 1;
	}
	return this.aCurve(i + ticks, t).toFixed( this.precision );	
}

exports.tC = tC;

