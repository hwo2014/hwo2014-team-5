// USAGE: var Vec2 = require('./Vec2.js').Vec2;


// Coder Coded - HWO BOT RACER -
// Authors: Juha Rantanen,
//          Joonas Ruuskanen,
//          Pekka Toiminen
//
// 2D Vector class
//
// v = new Vec2(i, j);
// v2 = v.copy();
//
// v.copyTo(v) - copies i and j to v.
// v.set(i,j) - set i and j
// v.r2d(r) - radians to degrees
// v.d2r(d) - degrees to radians
// v.mag(v) - return signed length.
// v.len( ) - length
// v.dot(v) - dot product with v
// v.dotSelf() - dot product with self
// v.angle(v) - Angle between this & v
// v.coangle(v) - PI - Angle between this & v
// v.makelen(l) - make this lenght l
// v.add(v, o) - this + v, output to o
// v.addSelf(v) - this + v, output to this
// v.sub(v, o) - this - v, output to o
// v.subSelf(v) - this - v, output to this
// v.scale(s) - multiply with scalar
// v.normalize() - make this len to 1
// v.orto() - make this orthogonal
// v.invert() - invert direction
// v.rotate(rad) - rotate radians (+ = left)
// v.dist(v) - distance between two vectors (cos law)
// 
//
// safe acos routine provided as sometimes rounding
// errors give us > 1.0 or < -1.0 args for Math.acos.

var Vec2 = function( i, j ) {
  this.i = i;
  this.j = j;
}

Vec2.prototype.set = function( i, j ) {
  this.i = i;
  this.j = j;
}

Vec2.prototype.acos = function( v ) {
  if (v > 1.0) {
    v = 1.0;
  } else if (v < -1.0) {
    v = -1.0;
  }
  return Math.acos( v );
}

Vec2.prototype.dist = function( v ) {
  return Math.sqrt( this.dotSelf() + v.dotSelf() - 2 * this.dot( v ) * Math.cos( this.angle( v ) ));
}

Vec2.prototype.copy = function() {
  return new Vec2( this.i, this.j );
}

Vec2.prototype.copyTo = function( v ) {
  v.i = this.i;
  v.j = this.j;
}

Vec2.prototype.r2d = function( r ) {
  return ( 180 / Math.PI ) * r;
}

Vec2.prototype.d2r = function( d ) {
  return ( Math.PI / 180 ) * d;
}

Vec2.prototype.mag = function( v ) {
  if ( Math.abs( this.angle( v ) ) <= Math.PI/2 ) {
    return this.len();
  } else {
    return -1 * this.len();
  }
}

Vec2.prototype.len = function() {
  return Math.sqrt( this.dotSelf() )
}

Vec2.prototype.add = function( v, o ) {
  o.i = this.i + v.i;
  o.j = this.j + v.j;
}

Vec2.prototype.sub = function( v, o ) {
  o.i = this.i - v.i;
  o.j = this.j - v.j;
}

Vec2.prototype.subSelf = function( v ) {
  this.i -= v.i;
  this.j -= v.j;
}

Vec2.prototype.addSelf = function( v ) {
  this.i += v.i;
  this.j += v.j;
}

Vec2.prototype.scale = function ( s ) {
  this.i *= s;
  this.j *= s;
}

Vec2.prototype.normalize = function() {
  this.scale( 1 / this.len() );  
}

Vec2.prototype.dot = function( v ) {
  return ( this.i * v.i + this.j * v.j );
}

Vec2.prototype.dotSelf = function() {
  return ( this.i * this.i + this.j * this.j );
}

Vec2.prototype.invert = function() {
  this.i *= -1;
  this.j *= -1;
}

Vec2.prototype.rotate = function ( t ) {
  // rotate in radians, use d2r to convert deg -> rad.
  // LEFT (-) RIGHT (+)
  // so we follow hwo specs
  var i = this.i * Math.cos( t ) - this.j * Math.sin( t );
  var j = this.i * Math.sin( t ) + this.j * Math.cos( t );
  this.i = i;
  this.j = j;
}

Vec2.prototype.orto = function() {
  var i = this.i;
  this.i = -1 * this.j;
  this.j = i;
}

Vec2.prototype.angle = function( v ) {
  // signed angle (0..PI, -PI..0) in radians between vectors
  // use r2d to convert rad -> deg.
  //return this.acos( this.dot( v ) / ( this.len() * v.len() ) );
  // HWO Specs: for right turns, this is positive, so we multiply with -1
  return -1 * (Math.atan2( v.j, v.i ) - Math.atan2( this.j, this.i ));
}

Vec2.prototype.coangle = function( v ) {
  // returns angle between this and
  // vector v, when v is placed
  // the end of this. 
  return (Math.PI - this.angle( v ));
}

Vec2.prototype.makeLen = function( l ) {
  this.normalize();
  this.scale( l );
}

// // test Vec2

// var a = new Vec2(10, 0);
// var b = a.copy();
// for (var i = 0; i < 360; i+=10) {
//   a.rotate(a.d2r(10));
//   console.log('i: %s, j: %s, len: %s, angle: %s', a.i.toFixed(2), a.j.toFixed(2), a.len().toFixed(2), a.r2d(a.angle(b)).toFixed(1));
// }

// // test orto
// a.set(9, 4);
// b.set(9, 4);
// console.log('angle should be 0: %s', (a.angle(b)));
// b.orto();
// console.log('angle should be 90: %s', (a.r2d(a.angle(b))));

// // test cosine law triangle:
// a.set(10, 0);
// b.set(0, 10);
// console.log('dist should be %s: %s ', Math.sqrt(2*10*10), a.dist( b ));

// // test distance / deltas
// c = new Vec2(0, 0);
// b.sub(a, c);
// console.log('to get from location a (10,0) to b (0,10): %si %sj', c.i, c.j);

exports.Vec2 = Vec2;