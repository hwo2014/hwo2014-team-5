/* Coder Coded - HWO BOT RACER - Authors: Juha Rantanen, Joonas Ruuskanen, Pekka Toiminen */

var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

// a script that's "out of our reach" checks for args etc. Don't touch above.

// =========================================================================
// CODE STARTS HERE
// =========================================================================

var yourCar, raceTrack, raceCars, raceSession;

var switchable = true;

// used for advanced testing
var trackName = 'Keimola';
var password = '';
var carCount = 3;

// Select race type -- NOTE: USE 'join' FOR AUTOMATED TEST RUNS!

var dataDict = { name: botName, key: botKey };
var raceDict = { msgType: 'join', data: dataDict };
//var raceDict = { msgType: 'joinRace', data: dataDict, trackName: trackName, password: password, carCount: carCount };
//var raceDict = { msgType: 'createRace', data: dataDict, trackName: trackName, password: password, carCount: carCount };

// Connect to server

client = net.connect( serverPort, serverHost, function() { return send( raceDict ) });
jsonStream = client.pipe(JSONStream.parse());

// Game loop

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    ctrl = naiveAI(data.data);
    // we get also data.gameId and data.gameTick here.
    if (ctrl.switchLane) {
      send({
        msgType: "switchLane",
        data: ctrl.targetLane
      })
    } else {
      send({
        msgType: "throttle",
        data: ctrl.throttle
      });
    }
  } else {
    if ( data.msgType === 'yourCar' ) {

      carColor = data.data.color;
      console.log('Our car color: ' + carColor);

    } else if ( data.msgType === 'gameInit' ) {
  
      raceTrack = data.data.race;      
      raceCars = data.data.cars;
      raceSession = data.data.raceSession;

      // tell trackAI that our track is here

    } else if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else {
      console.log(data);
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected due to stream error - not our code");
});

function naiveAI(data) {
  
  var ctrl = {
    switchLane: false,
    targetLane: 'Left',
    throttle: 1.0
  }

  var ourPosition;

  for ( var i = 0; i < data.length; i++ ) {
    if ( data[i].id.color == carColor ) {
      ourPosition = data[i];
    }
  
    // we can also track other cars here
  }

  // we have:
  // ourPosition.angle
  // ourPosition.piecePosition
  //                          .pieceIndex
  //                          .inPieceDistance
  //                          .lap
  //                          .lane
  //                               .startLaneIndex
  //                               .endLaneIndex
  //
  // check that we are on the innermost lane...
  // lanes are described in raceTrack.lanes
  // we must check from track description what is the closest to center!

  if (ourPosition.piecePosition.lane.startLaneIndex == 0 && switchable) {
    ctrl.targetLane = 'Right';
    ctrl.switchLane = true;
    switchable = false;
  }

  // just check that when we are close to curves, drop throttle to
  // 0.6 and on straigths put it to 1.0
  
  if (ourPosition.angle > 5) {
    ctrl.throttle = 0.3;
  } else {
    ctrl.throttle = 0.7;
  }

  return ctrl;


}