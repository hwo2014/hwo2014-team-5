// Fuzzy Controller - The brain of our car
//var _ = require('./Fuzzy.js');
var FuzzySet = require('./FuzzySet.js').FuzzySet;

var Brain = function( precision, debug_msg, debug_phys ) {
  this.precision = precision;
  this.debug_msg = debug_msg;
  this.debug_phys = debug_phys;
  this.initQualify = true;
  this.initRace = false;
  this.noEnemies = true;

  // do here generic initialization

  this.flagFalse = false;
  this.flagTrue = true;
};

var proto = Brain.prototype;

proto.think = function( me, enemies, track ) {
  // called when qualify starts!
  if ( this.initQualify ) {
    console.log( 'We have physics - create fuzzy rules for qualify');
    this.initFuzzySets( me, enemies, track );
    this.initVariables();
    this.initQualify = false;
  }

  if ( this.initRace ) {
    // called when race starts!
    console.log(' BRAIN INIT RACE ');
    this.initVariables();
    this.initRace = false;
    this.inRace = true;
  }

  //var old_maxthrottle = this.doTheMaxThrottle();

  // You can also check for switchpieces in range! 5 ticks.. 
  // During tick change currentLane to targetlane and start throttling it!
  // calculate also ticks to crash if current angular speed & angular acc hold up
  // if you can exit a corner without crashing then keep full throttle.
  
  // PELIN SÄÄNNÖT:
  //
  // Ollaan suoralla:
  // Auto haistelee koko ajan tuleeko radalla säteen muutoksia (jerk) vastaan
  // nykyisellä nopeudella ja throttlella ajettaessa this.tickRadar - tickin kuluessa.
  // jos ei, niin FULL THROTTLE
  //
  // Ollaan suoralla:
  // this.tickRadar:in kuluessa tulee jerkki vastaan. Kontrolli siirretään
  // fuzzy-rulesetille joka hanskaa jerkin vastaanottoa.
  // 
  // Ollaan mutkassa:
  // ei seuraavaa jerkkiä this.tickRadar:in kuluessa. Kontrolli siirretään
  // fuzzy-rulesetille joka hanskaa mutkassa radalla pysymistä.
  //
  // Ollaan mutkassa:
  // seuraava jerkki on joko vastapuolen kurviin, tai suoralle.
  // Katotaan jos nykyinen kulmanopeus ja kulmakiihtyvyys riittää siihen että
  // jerkki saavutettaessa kulma ei kasva yli 60 astetta, niin FULL THROTTLE.
  // Jos nykyinen kulmakiihtyvyys ja kulmanopeus vaikuttaa siltä että auto
  // kääntyy yli 60 astetta ennen jerkin saavuttamista, niin kontrolli
  // siirretään fuzzy-rulesetille joka hanskaa mutkassa kriittistä ajoa. 
  //
  // Ollaan mutkassa:
  // seuraava jerkki on saman suunnan loivempaan kurviin, TÄÄ PITÄÄ TESTAA,
  // MUTTA MUN INTUITIO SANOO ETTÄ SE SOPII TOHON YLEMPÄÄN CASEEN.
  //
  // Ollaan mutkassa:
  // seuraava jerkki on saman suunnan jyrkempään kurviin, pidetään kontrolli fuzzy
  // setillä joka hanskaa kurvissa siirtymää jyrkempään samansuuntaiseen kurviin
  // 

  // jos crashataan ja syynä oli meidän liian suuri nopeus, niin meidän pitää
  // säätää kyseistä fuzzy-rulesettiä JA kasvattaa this.tickRadar:in arvoa esim.
  // 5:llä tai 10:llä.
  //

  var distToJerk = me.nextDisturbance[0];
  var distInTicks = me.distanceTraveledInTicks( me.throttle, 0.0, this.tickRadar, me.vel );
  var jerkDiff = me.nextDisturbance[1];
  var jerkRadius = me.nextDisturbance[2];
  var jerkAngle = me.nextDisturbance[3];
  var jerkPieceLength = me.nextDisturbance[4];
  this.tickRadar = Math.round(me.vel * 3 + 1);
  var closestEnemies = me.getClosestEnemies();

  if (me.inSwitch) {
    console.log('switching lanes');
  }
  if ( me.pieceRadius == 0) {
    this.jerkTick = 0;
    this.jerkTickAdder = 0;
    // Ollaan suoralla
    // Tuleeko jerkkejä vastaan nykyisellä nopeudella ja throttlella
    // tickRadar tickien kuluessa? Jos ei tuu, niin full throttle.
    // ei hanskaa lyhyitä suoria!
    if ( distInTicks < distToJerk && this.noJerkMode ) {
      if ( this.survivesStraight( distToJerk, me, track, 1.0, 3 ) ) {
        console.log('No Jerks: Full throttle');
        me.throttle = 1.0;
      } else {
        console.log('HÄTÄJARRUTUS: (spinnataan ihan just)');
        me.throttle = 0.0;
      }
    } else {
    // Tulee jerkki vastaan nykyisellä nopeudella ja throttlella
    // tickRadar tickien kuluessa. Siirretään kontrolli jerkkiä vastaanottavalle
    // fuzzy-rulesetille.
      this.noJerkMode = false;
      // tän pitää hanskaa hyvä attack speed mutkaan!
      console.log('FuzzyJerk is handling gas now');
      // me.throttle = this.defuzzification( this.fuzzyJerkRules( me, enemies, track, jerkRadius, distInTicks ));
      // if (me.vel < 7) {
      //   console.log('velocity is smaller than 5:%s', me.vel);
      //   me.throttle = me.throttleForTerminalVelocity(8);
      // } else {
      //   me.throttle = 0.0;
      // }
      me.throttle = this.maxThrottle( me, track, distToJerk, jerkRadius, jerkPieceLength );
    }
  } else {
    this.noJerkMode = true;
    if ( me.p_pieceRadius != me.pieceRadius ) {
      this.jerkTick = 0;
      this.jerkTickAdder = 1;
    }
    this.jerkTick += this.jerkTickAdder;
    // Ollaan mutkassa
    // Ei seuraavaa jerkkiä nykyisellä nopeudella ja throttlella.
    // Kontrolli mutkassa pysymistä hoitavalla fuzzy kontrollilla.
    if ( distInTicks < distToJerk ) {
      if ( this.jerkTick > 1 && this.survivesCurve( distToJerk, me, track, 1.0, this.tickRadar )) {
        // mutkaan tultaessa tulee yleensä paha jerkki, kaks tickiä alusta
        // on sellasia että annetaan fuzzycurven ajaa.
        console.log('In Curve - can make it with FULL throttle');
        me.throttle = 1.0;
      } else if ( this.jerkTick <= 1 ) {
        console.log('JERKTICK: brake');
        me.throttle = 0.0;
      } else {
        console.log('In Curve - CAN NOT make with full - using predictor');
        me.throttle = this.inCurveSpeed( me, enemies, track, distToJerk, 5 );
        //me.throttle = this.defuzzification( this.qualifyThrottleRules( me, enemies, track ), 'throttle' );;        
      } 
    } else {
    // Tulee jerkki vastaan nykyisellä nopeudella ja throttlella
    // tickRadar tickien kuluessa. Katsotaan aluksi mihin suuntaan jerkki on
    // onko se suoralle tai vastamutkaan?
      if ((jerkRadius == 0) || 
          (jerkAngle < 0 && me.pieceAngle > 0) ||
          (jerkAngle > 0 && me.pieceAngle < 0)) {
        // jerkki on suoralle tai vastamutkaan, katsotaan riittävätkö tickit
        // nykyisellä kulmanopeudella ja kulkmakiihtyvyydellä, jos throttle on 1.0
        // var crashTicks = this.calcCrashTicks( me, track );
        // var distInCrashTicks = me.distanceTraveledInTicks( 1.0, 0.0, crashTicks, me.vel );
        if (this.survivesCurve( distToJerk, me, track, 1.0, this.tickRadar ) ) {
          // kuitenkin jos kyseessä on lyhyt pala ( alle tickRadarin mittainen
          // nykynopeudella), niin pakotetaan hanskaus fuzzycurvelle
          if ( jerkRadius == 0 && jerkPieceLength < me.distanceTraveledInTicks( this.p_throttle, 0.0, this.tickRadar , me.vel )) {
            // pakota fuzzycurvella ajo
            console.log('ENFORCED FuzzyCurve is handling gas now - using fuzzy');
            //me.throttle = this.defuzzification( this.fuzzyCurveRules( me, enemies, track ), 'throttle' );
              me.throttle = this.inCurveSpeed( me, enemies, track, distToJerk, 3 );
          } else {
            // riittävät, full throttle
            console.log('Jerk is straight/opposite curve - can handle - full throttle %s', jerkDiff);
            me.throttle = 1.0;
          } 
        } else {
          // ei riitä, kontrolli siirtyy criticalCurveDrive fuzzy setille
          if (jerkRadius == 0) {
            console.log('HÄTÄJARRUTUS - seuraavana suora');
          } else {
            console.log('HÄTÄJARRUTUS - seuraavana vastamutka');
          }
          me.throttle = 0.0;
        }
      } else if (( me.pieceAngle > 0 && jerkAngle > 0 && jerkAngle < me.pieceAngle ) ||
                 ( me.pieceAngle < 0 && jerkAngle < 0 && jerkAngle > me.pieceAngle )) {
        // jerkki on loivempaan samansuuntaiseen mutkaan, tehdään aluksi samoin
        // kuin yllä, muutetaan koodia jos tarvii
        // var crashTicks = this.calcCrashTicks( me, track );
        // var distInCrashTicks = me.distanceTraveledInTicks( 1.0, 0.0, crashTicks, me.vel );
        if ( this.survivesCurve( distToJerk, me, track, 1.0, 5 ) ) {
          // riittävät, full throttle
          console.log('Jerk on loivempi samansuuntainen - can handle - full throttle');
          me.throttle = 1.0;
        } else {
          // ei riitä, ainut keino on jarruttaa.
          console.log('HÄTÄJARRUTUS!');
          me.throttle = 0.0;
        }
      } else {
        // jerkki on jyrkempään samansuuntaiseen mutkaan, siirretään kontrolli
        // fuzzysetille joka hanskaa jerkin jyrkempään samansuuntaiseen mutkaan
        console.log('FuzzyCurve handles gas, Jerk on jyrkempi samansuuntainen')
        me.throttle = this.maxThrottle( me, track, distToJerk, jerkRadius, jerkPieceLength );
      }
    }
  }

  //me.throttle = this.defuzzification( this.qualifyThrottleRules( me, enemies, track ), 'throttle' );
 
  // Sanity check
  if (me.throttle > 1.0) {
    me.throttle = 1.0
  } else if (me.throttle < 0.0) {
    me.throttle = 0.0
  }

  if (this.debug_phys) {
    if (me.throttle == 1.0) {
      console.log('d:%s v:%s a:%s t:FULL θ:%s ω:%s α:%s r:%s e:%s w:%s',
                me.inPieceDistance.toFixed(2), me.vel.toFixed(3), me.acc.toFixed(3), 
                me.angle.toFixed(3), me.aVel.toFixed(3), me.aAcc.toFixed(3), me.pieceRadius.toFixed(1), me.pieceAngle.toFixed(1), me.inSwitch);
    } else {
      if ( me.acc < 0 ) {
        var afix = 2;
      } else {
        var afix = 3;
      }
      console.log('d:%s v:%s a:%s t:%s θ:%s ω:%s α:%s r:%s e:%s w:%s',
                me.inPieceDistance.toFixed(2), me.vel.toFixed(3), me.acc.toFixed(afix), me.throttle.toFixed(2), 
                me.angle.toFixed(3), me.aVel.toFixed(3), me.aAcc.toFixed(3), me.pieceRadius.toFixed(1), me.pieceAngle.toFixed(1), me.inSwitch);
    }
  }
  

  if ( !me.goingToSwitch ) {
    // SWITCHLANEN SÄÄNNÖT:
    // Lane switch lähetetään vain silloin kun ei olla kriittisessä tilanteessa,
    // tämä tarkoittaa sitä että throttle on asetettu full:iksi.
    //
    if ( me.throttle == 1 ) { 
      var lanes = track.getShortestLanes( me.pieceIndex, me.currentLane, me.leftLanes, me.rightLanes );
      me.switchLane = lanes[0].dir;
      if ( closestEnemies.ahead.keep < closestEnemies.ahead.left || closestEnemies.ahead.keep < closestEnemies.ahead.right ) {
        me.switchLane = lanes[1].dir;
      }
      if ( me.switchLane ) {
        me.goingToSwitch = true;
      } else {
        me.goingToSwitch = false;
      }
    }
  }
  if ( !me.inTurbo && me.turboAvailable ) {
    // turbon käyttösäännöt:
    var effect = this.turboEffect( me, track, this.p_throttle );
    // tätä käytetään vain pitkillä suorilla nyt
    //console.log('test turbo: distToJerk: %s, turboTravel:%s, pieceRadius:%s, p_throttle:%s', distToJerk, effect.turboTravel, me.pieceRadius, this.p_throttle);
    if ( distToJerk > 0.8*effect.turboTravel && me.pieceRadius == 0 && this.p_throttle > 0 && me.throttle > 0) {
      me.useTurbo = true;
    } else if (closestEnemies.ahead.keep < 3*me.length && distToJerk < 5*me.length) {
      // bump!
      me.useTurbo = true;
    }
  }
  
  if (me.vel < 2.0) {
    me.throttle = 1.0
  } else if (me.vel < 5.0) {
    me.throttle = me.throttleForTerminalVelocity(8.0)
  } else if (me.vel < 6.0) {
    me.throttle = me.throttleForTerminalVelocity(7.0)
  }

  this.p_throttle = me.throttle;
};

proto.inCurveSpeed = function( me, enemies, track, distToJerk, extraTicks ) {
  var throttle = 1.0;
  s = this.survivesCurve( distToJerk, me, track, throttle, extraTicks );
  while ( s == false ) {
    throttle += 0.05;
    if ( 2.0 - throttle <= 0 ) {
      return 0.0;
    }
    s = this.survivesCurve( distToJerk, me, track, throttle, extraTicks );
  }
  return 2.0 - throttle;
}


proto.fuzzyJerkRules = function( me, enemies, track, jerkRadius, distInTicks ) {
  var vel = this.fuzzySpeed;
  var t = this.fuzzyThrottle;
  var rad = this.fuzzyRadius;
  //var d = this.fuzzyDistInTicks;

  vel.crisp = me.vel;
  t.crisp = me.throttle;
  rad.crisp = jerkRadius;
  //d.crisp = distInTicks;

  return ([
     rad.IS('r0').AND(vel.IS(['hi'])).ACT(t._('lo'))
    ,rad.IS('r0').AND(vel.IS(['med'])).ACT(t._('lo'))
    ,rad.IS('r0').AND(vel.IS(['lo'])).ACT(t._('lo'))

    ,rad.IS('r1').AND(vel.IS(['hi'])).ACT(t._('lo'))
    ,rad.IS('r1').AND(vel.IS(['med'])).ACT(t._('lo'))
    ,rad.IS('r1').AND(vel.IS(['lo'])).ACT(t._('med'))

    ,rad.IS('r2').AND(vel.IS(['hi'])).ACT(t._('lo'))
    ,rad.IS('r2').AND(vel.IS(['med'])).ACT(t._('med'))
    ,rad.IS('r2').AND(vel.IS(['lo'])).ACT(t._('med'))

    ,rad.IS('r3').AND(vel.IS(['hi'])).ACT(t._('lo'))
    ,rad.IS('r3').AND(vel.IS(['med'])).ACT(t._('med'))
    ,rad.IS('r3').AND(vel.IS(['lo'])).ACT(t._('med'))

    ,rad.IS('r4').AND(vel.IS(['v0'])).ACT(t._('t9'))
    ,rad.IS('r4').AND(vel.IS(['v1'])).ACT(t._('t9'))
    ,rad.IS('r4').AND(vel.IS(['v2'])).ACT(t._('t9'))
    ,rad.IS('r4').AND(vel.IS(['v3'])).ACT(t._('t9'))
    ,rad.IS('r4').AND(vel.IS(['v4'])).ACT(t._('t9'))
    ,rad.IS('r4').AND(vel.IS(['v5'])).ACT(t._('t9'))
    ,rad.IS('r4').AND(vel.IS(['v6'])).ACT(t._('t0'))
    ,rad.IS('r4').AND(vel.IS(['v7'])).ACT(t._('t0'))
    ,rad.IS('r4').AND(vel.IS(['v8'])).ACT(t._('t0'))
    ,rad.IS('r4').AND(vel.IS(['v9'])).ACT(t._('t0'))

    // tarvitaan paljon tarkempi kontrolli velocityyn
    // se pyörii niin paljon 5 - 9 välissä!!
    ]);
}

proto.qualifyThrottleRules = function( me, enemies, track ) {
  // update the crisp values for fuzzy sets
  var v = this.fuzzySpeed;
  var t = this.fuzzyThrottle;
  var ang = this.fuzzyAngle;
  var aVel = this.fuzzyAngularSpeed;
  var aAcc = this.fuzzyAngularAcceleration;
  //var bend = this.fuzzyTightness;

  v.crisp = me.vel;
  t.crisp = me.throttle;
  ang.crisp = me.angle;
  aVel.crisp = me.aVel;
  aAcc.crisp = me.aAcc;
  me.tightnessScope = 100; // this could be adjusted to speed (e.g. 25-30 ticks)
//  bend.crisp = me.curveTightness;
  // next disturbance has distance to it, its difference to current, and absolute value
  // difference > we know that it is a more loose curve


  // return fuzzy values, below are our rules use ISNOT, AND, OR, NOT and ACT
  // speed names: ['super low', 'low', 'safe', 'high', 'super high']
  // angle names: ['straight','low','med','high'],
  // throttle names: ['brake', 'slow', 'safe', 'fast', 'full'],

  return ([
    ang.IS('0').ACT(t._('t0'))
    ,(ang.IS('l5').OR(ang.IS('r5'))).ACT(t._('t0'))
    ,(ang.IS('l10').OR(ang.IS('r10'))).ACT(t._('t0'))
    ,(ang.IS('l15').OR(ang.IS('r15'))).ACT(t._('t0'))
    ,(ang.IS('l20').OR(ang.IS('r20'))).ACT(t._('t0'))
    ,(ang.IS('l25').OR(ang.IS('r25'))).ACT(t._('t0'))
    ,(ang.IS('l30').OR(ang.IS('r30'))).ACT(t._('t0'))
    ,(ang.IS('l35').OR(ang.IS('r35'))).ACT(t._('t0'))
    ,(ang.IS('l40').OR(ang.IS('r40'))).ACT(t._('t0'))
    ,(ang.IS('l45').OR(ang.IS('r45'))).ACT(t._('t0'))
    ,(ang.IS('l50').OR(ang.IS('r50'))).ACT(t._('t0'))
    ,(ang.IS('l55').OR(ang.IS('r55'))).ACT(t._('t0'))
    // Angle grows fast, and our angle 
    // ,aAcc.IS('rhi').AND(ang.IS('rlo')).ACT(t._('lo'))
    // ,aAcc.IS('rhi').AND((ang.IS('rmed')).OR(ang.IS('rhi'))).ACT(t._('t0'))
    // ,aAcc.IS('rmed').AND((ang.IS('rlo')).OR(ang.IS('rmed'))).ACT(t._('med'))
    // ,aAcc.IS('rlo').AND(ang.IS('rlo')).ACT(t._('hi'))
    // ,aAcc.IS('rlo').AND((ang.IS('rmed')).OR(ang.IS('rhi'))).ACT(t._('med'))
    // ,aAcc.IS('llo').AND(ang.IS('rhi')).ACT(t._('med'))
    // ,aAcc.IS('llo').AND(ang.IS('rmed')).ACT(t._('hi'))
    // ,aAcc.IS('llo').AND(ang.IS('rlo')).ACT(t._('hi'))
    // ,(ang.IS(['r45', 'r50','r55']).AND(aVel.IS('0'))).HIT(me.throttleForTerminalVelocity(me.vel))
    // ,ang.IS('rhi').AND(aVel.IS('rlo')).ACT(t._('t1'))
    // ,ang.IS('rhi').AND(aVel.IS('rmed')).ACT(t._('t2'))
    // ,ang.IS('rhi').AND(aVel.IS('rhi')).ACT(t._('t0'))
    // ,ang.IS('rmed').AND(aVel.IS('0')).ACT(t._('t6'))
    // ,ang.IS('rmed').AND(aVel.IS('rlo')).ACT(t._('t2'))
    // ,ang.IS('rmed').AND(aVel.IS('rmed')).ACT(t._('t3'))
    // ,ang.IS('rmed').AND(aVel.IS('rhi')).ACT(t._('t1'))
    // ,ang.IS('rlo').AND(aVel.IS('0')).ACT(t._('t9'))
    // ,ang.IS('rlo').AND(aVel.IS('rlo')).ACT(t._('t9'))
    // ,ang.IS('rlo').AND(aVel.IS('rmed')).ACT(t._('t4'))
    // ,ang.IS('rlo').AND(aVel.IS('rhi')).ACT(t._('t2'))
    //,ang.IS(['r50', 'r55']).AND(aAcc.IS('rlo')).HIT(me.throttleForTerminalVelocity(me.vel))
     

    // ,ang.IS('llo').AND
    // ,ang.IS('lhi').AND((avel.IS('rmed')).OR(avel.IS('rhi'))).ACT(t._('hi'))
    // ,ang.IS('rhi').AND((avel.IS('lmed')).OR(avel.IS('lhi'))).ACT(t._('hi'))
    // ,ang.IS('lo').AND(avel.IS('lo')).ACT(t._('hi'))
    
  ]);
};

proto.singleSided5Divisions = function(name) {
  return (new FuzzySet({
    name: name,
    crisp: 0,
    crispMin: 0,
    crispMax: 1.0,
    names: ['l2', 'l1', '0', 'r1', 'r2'],
    sets: [[0.15, 0.15, 0.25]
          ,[0.2, 0.35, 0.5]
          ,[0.35, 0.5, 0.65]
          ,[0.5, 0.65, 0.8]
          ,[0.65, 0.85, 0.85]]
  }));
}

proto.initFuzzySets = function ( me, enemies, track ) {
  this.fuzzyRadius = this.singleSided5Divisions('fuzzyRadius');
  this.fuzzyRadius.names = ['r0', 'r1', 'r2', 'r3', 'r4'];
  var coeff = track.getAverageRadius();
  for ( var i = 0; i < this.fuzzyRadius.sets.length; i++) {
    this.fuzzyRadius.sets[i][0] *= coeff;
    this.fuzzyRadius.sets[i][1] *= coeff;
    this.fuzzyRadius.sets[i][2] *= coeff;
  }
  console.log("Average radius: %s", coeff);
  this.crispMin = 0;
  this.crispMax = coeff;

  // do the qualify round initalization
  var maxVel = me.terminalVelocity(1.0);
  this.fuzzySpeed = new FuzzySet({
    name: 'speed',
    crisp: me.vel,
    crispMin: 0.0,
    crispMax: maxVel,
    names: ['v0', 'v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'lo', 'med', 'hi'],
    sets: [[0.0, 0.0, 0.1]
          ,[0.0, 0.1, 0.2]
          ,[0.1, 0.2, 0.3]
          ,[0.2, 0.3, 0.4]
          ,[0.3, 0.4, 0.5]
          ,[0.4, 0.5, 0.6]
          ,[0.5, 0.6, 0.7]
          ,[0.7, 0.8, 0.9]
          ,[0.8, 0.9, 1.0]
          ,[0.9, 1.0, 1.0]
          ,[0.0, 0.0, 5.0]
          ,[3.5, 5.0, 7.5]
          ,[5.0, 7.5, 7.5]]
  });
  for ( var i = 0; i < this.fuzzySpeed.sets.length; i++ ) {
    this.fuzzySpeed.sets[i][0] *= maxVel;
    this.fuzzySpeed.sets[i][1] *= maxVel;
    this.fuzzySpeed.sets[i][2] *= maxVel;
  }

  this.fuzzyAngularAcceleration = new FuzzySet({
    name: 'angularAcceleration',
    crisp: me.aVel,
    crispMin: -1.0,
    crispMax: 1.0,
    names: ['lhi', 'lmed', 'llo', 'rlo', 'rmed', 'rhi'],
    sets: [[-1.0, -1.0, -0.3]
          ,[-0.4, -0.2, -0.1]
          ,[-0.15, -0.08, 0.0, 0.01]
          ,[-0.01, 0.0, 0.08, 0.15]
          ,[0.1, 0.2, 0.4]
          ,[0.3, 1.0, 1.0]]
  });

  this.fuzzyAngularSpeed = new FuzzySet({
    name: 'angularSpeed',
    crisp: me.aVel,
    crispMin: -10.0,
    crispMax: 10.0,
    names: ['l55', 'l50', 'l45', 'l40', 'l35', 'l30', 'l25', 'l20', 'l15', 'l10', 'l5', '0',
            'r5', 'r10', 'r15', 'r20', 'r25', 'r30', 'r35', 'r40', 'r45', 'r50', 'r55', 'lhi', 'lmed', 'llo', 'rlo', 'rmed', 'rhi'],
    sets: [[-59.999, -59.999, -50.0]
          ,[-55.0, -50.0, -45.0]
          ,[-50.0, -45,0, -40.0]
          ,[-45.0, -40.0, -35.0]
          ,[-40.0, -35.0, -30.0]
          ,[-35.0, -30.0, -25.0]
          ,[-30.0, -25.0, -20.0]
          ,[-25.0, -20.0, -15.0]
          ,[-20.0, -15.0, -10.0]
          ,[-15.0, -10.0, -5.0]
          ,[-10.0, -5.0, 0.0]
          ,[-5.0, 0.0, 5.0]
          ,[0.0, 5.0, 10.0]
          ,[5.0, 10.0, 15.0]
          ,[10.0, 15.0, 20.0]
          ,[15.0, 20.0, 25.0]
          ,[20.0, 25.0, 30.0]
          ,[25.0, 30.0, 35.0]
          ,[30.0, 35.0, 40.0]
          ,[35.0, 40.0, 45.0]
          ,[40.0, 45.0, 50.0]
          ,[45.0, 50.0, 55.0]
          ,[50.0, 59.999, 59.999]
          ,[-59,999, -59.999, -40.0]
          ,[-45.0, -35.0, -25.0]
          ,[-30.0, -15.0, 0.0, 0.01]
          ,[-0.01, 0.0, 15.0, 30.0]
          ,[25.0, 35.0, 45.0]
          ,[40.0, 59.999, 59.999]]
  });
  var aVelCoeff = 59.999/9;
  for ( var i = 0; i < this.fuzzyAngularSpeed.length; i++ ) {
    this.fuzzyAngularSpeed.sets[i][0] *= aVelCoeff;
    this.fuzzyAngularSpeed.sets[i][1] *= aVelCoeff;
    this.fuzzyAngularSpeed.sets[i][2] *= aVelCoeff;
  }

  this.fuzzyAngle = new FuzzySet({
    name: 'angle',
    crisp: me.angle,
    crispMin: -59.999,
    crispMax: 59.999,
    names: ['l55', 'l50', 'l45', 'l40', 'l35', 'l30', 'l25', 'l20', 'l15', 'l10', 'l5', '0',
            'r5', 'r10', 'r15', 'r20', 'r25', 'r30', 'r35', 'r40', 'r45', 'r50', 'r55', 'lhi', 'lmed', 'llo', 'rlo', 'rmed', 'rhi'],
    sets: [[-59, -59, -50.0]
          ,[-55.0, -50.0, -45.0]
          ,[-50.0, -45,0, -40.0]
          ,[-45.0, -40.0, -35.0]
          ,[-40.0, -35.0, -30.0]
          ,[-35.0, -30.0, -25.0]
          ,[-30.0, -25.0, -20.0]
          ,[-25.0, -20.0, -15.0]
          ,[-20.0, -15.0, -10.0]
          ,[-15.0, -10.0, -5.0]
          ,[-10.0, -5.0, 0.0]
          ,[-5.0, 0.0, 5.0]
          ,[0.0, 5.0, 10.0]
          ,[5.0, 10.0, 15.0]
          ,[10.0, 15.0, 20.0]
          ,[15.0, 20.0, 25.0]
          ,[20.0, 25.0, 30.0]
          ,[25.0, 30.0, 35.0]
          ,[30.0, 35.0, 40.0]
          ,[35.0, 40.0, 45.0]
          ,[40.0, 45.0, 50.0]
          ,[45.0, 50.0, 55.0]
          ,[50.0, 59, 59]
          ,[-59,999, -59.999, -40.0]
          ,[-45.0, -35.0, -25.0]
          ,[-30.0, -15.0, 0.0, 0.01]
          ,[-0.01, 0.0, 15.0, 30.0]
          ,[25.0, 35.0, 45.0]
          ,[40.0, 59.999, 59.999]]
  });

  this.fuzzyThrottle = new FuzzySet({
    name: 'throttle',
    crisp: me.throttle,
    crispMin: 0.0,
    crispMax: 1.0,
    names: ['t0', 't1', 't2', 't3', 't4', 't5', 't6', 't7', 't8', 't9', 'lo', 'med', 'hi'],
    sets: [[0.0, 0.0, 0.1]
          ,[0.0, 0.1, 0.2]
          ,[0.1, 0.2, 0.3]
          ,[0.2, 0.3, 0.4]
          ,[0.3, 0.4, 0.5]
          ,[0.4, 0.5, 0.6]
          ,[0.5, 0.6, 0.7]
          ,[0.7, 0.8, 0.9]
          ,[0.8, 0.9, 1.0]
          ,[0.9, 1.0, 1.0]
          ,[0.0, 0.0, 0.5]
          ,[0.4, 0.5, 0.6, 0.7]
          ,[0.55, 1.0, 1.0]]
  });
};

proto.turboEffect = function ( me, track, throttle ) {
  var coeff = me.turbo.factor;
  var ticks = me.turbo.duration;
  var turbotravel = me.distanceTraveledInTicks( throttle * coeff, 0.0, ticks, me.vel );
  var endvelocity = me.speedInTicks( ticks, throttle * coeff, me.vel );
  var tickstoback = me.ticksNeededForSpeed( me.vel, 0.0, endvelocity );
  var disttoback = me.distanceTraveledInTicks( throttle, 0.0, tickstoback, endvelocity );
  return {
    turboTravel: turbotravel,
    endVelocity: endvelocity,
    turboTicks: ticks,
    ticksToBack: tickstoback,
    distToBack: disttoback
  }
}

// proto.turboPossible = function ( me, track, throttle ) {
//   var coeff = me.turbo.factor;
//   var ticks = me.turbo.duration;
//   var turbotravel = me.distanceTraveledInTicks( throttle * coeff, 0.0, ticks, me.vel );
//   var endvelocity = me.speedInTicks( ticks, throttle * coeff, me.vel );

//   var d = track.getDistanceToNextCurve( me.pieceIndex, me.inPieceDistance, me.currentLane );
//   var maxcurvespeed = me.getMaxCurveSpeed( d[1].radius );
//   var distancetocurve = d[0];
//   var tickstobrake = me.ticksNeededForSpeed( maxcurvespeed, 0.0, endvelocity );
//   var distancetobrake = me.distanceTraveledInTicks( 0.0, 0.0, tickstobrake, endvelocity );

//   var ret = {}
//   // TODO: more sophisticated analysis including enemy pushes!
//   console.log( 'turbotravel: %s, distancetobrake: %s, distancetocurve: %s', turbotravel, distancetobrake, distancetocurve);
//   if ( me.pieceRadius == 0 && (distancetocurve > ( turbotravel + distancetobrake ) )) {
//     ret.boost = true;
//     ret.push = false;
//     ret.switchmiss = false;
//   } else {
//     ret.boost = false;
//     ret.push = false;
//     ret.switchmiss = false;
//   }
//   return ret;
// }


proto.initVariables = function() {
  // do something if we need to adjust for race
  this.tickRadar = 15;
  this.crashAngle = 59;
  this.errorTolerance = 0;

  this.jerkTick = 0;
  this.jerkTickAdder = 0;

  this.p_throttle = 0;
  this.noJerkMode = true;
};

proto.defuzzification = function( v, name ) {
	var nom = 0;
  var den = 0;
  for ( var i = 0; i < v.length; i++ ) {
  	if ( v[i][2] ) {
      return v[i][0];
    }
    nom += v[i][0]*v[i][1];
 		den += v[i][0];
  }
  if (den != 0) {
	  return nom/den;
	} else {
		console.log( 'denominator became zero - improve fuzzy ranges for %s!', name );
		return 1.0;
	}
};

proto.updateMaxMin = function( s, v ) {
	if ( s.crispMax < v ) {
		s.crispMax = v;
	}
	if ( s.crispMin > v ) {
		s.crispMin = v;
	}
}

proto.survivesStraight = function( distToJerk, me, track, throttle, extraTicks ) {
  // lasketaan vaan liikkuuko kulma kohti nollaa vai ei
  if ( me.angle < 0 && me.aVel < 0 && me.aAcc < 0) {
    return false;
  }

  if ( me.angle > 0 && me.aVel > 0 && me.aAcc > 0) {
    return false;
  }

  return true;
}

proto.survivesCurve = function( distToJerk, me, track, throttle, extraTicks ) {
  if ( me.angle == 0 || me.aVel == 0 || me.aAcc == 0 ) {
    return true;
  }
  var crashTicks = this.calcCrashTicks( me, track, extraTicks );
  if (crashTicks >= 300) {
    console.log('CRASHTICKS = 300');
    return true;
  }
  if (crashTicks < 0) {
    return false;
  }
  var distInCrashTicks = me.distanceTraveledInTicks( throttle, 0.0, crashTicks, me.vel );
  if ( distInCrashTicks > distToJerk + this.errorTolerance ) {
    return true;
  } else {
    return false;
  }
}

proto.doTheMaxThrottle = function() {
  // do the thinking ( separate it to throttle, turboUse, switchlane )
  // calcualte fuzzy values
  var throttle = this.maxThrottle( me, track );
  if ( throttle < 0.0 ) {
    throttle = 0.0;
  } else if ( throttle > 1.0 ) {
    throttle = 1.0;
  }
  return throttle;
}

proto.calcCrashTicks = function( me, track, extraTicks ) {
  // lasketaan montako tickiä kestää siihen että auto crashaa nykyisellä
  // kulmanopeudella ja kulmakiihtyvyydellä
  var angle = me.angle;
  var aVel = me.aVel;
  var aAcc = me.aAcc; // try to predict aAcc better..
  //aAcc += Math.abs(me.aJerk);
  
  var ticks = 0;
  while (( angle < this.crashAngle && angle > -this.crashAngle )) {
    ticks += 1;
    aVel += aAcc;
    angle += aVel;
    if (ticks >= 300) {
      return ticks;
    }
  }

  return ticks - extraTicks;
}

proto.maxThrottle = function ( me, track, distToJerk, jerkRadius, jerkPieceLength ) {
  // calculate the max throttle just to run well in empty races
  // d[0] = distance, d[1] = lanepiece
  //var d = track.getDistanceToNextCurve( me.pieceIndex, me.inPieceDistance, me.currentLane );

  // calc attack speed for next curve
  // tätä voi parantaa jos lasketaan mukaan kaaren pituus,
  // jolloin lyhyissä kurveissa nopeutta voi noistaa!
  var s = jerkRadius / ((1.5 / me.k ));
  if ( me.vel > s ) {
    var ticks = me.ticksNeededForSpeed( s, 0.0, me.vel );
    var travel = me.distanceTraveledInTicks( 1.0, 0.0, ticks, s );
    //console.log('ticks: %s, travel: %s, distance: %s', ticks, travel, d[0]);
    if ( travel > distToJerk ) {
      return 0.0;
    } else {
      return 1.0;
    }
  } else {
    return 1.0;
  }
}
  //console.log( 'curve attack speed: %s, distance to curve: %s, current velocity: %s', s, d[0], me.vel );

//   if ( me.pieceRadius != 0 ) {
//     var s = me.getMaxCurveSpeed( me.pieceRadius );
//     if ( me.vel < s ) {
//       if ( me.aAcc < 1.4) {
//         return 1.0;
//       }
//       return me.throttleForTerminalVelocity( s );
//     } else {
//       return 0.0;
//     }
//   }

//   // calc ticks needed to gain that attack speed
//   if ( me.vel > s ) {
//     var ticks = me.ticksNeededForSpeed( s, 0.0, me.vel );
//     var travel = me.distanceTraveledInTicks( 1.0, 0.0, ticks, s );
//     //console.log('ticks: %s, travel: %s, distance: %s', ticks, travel, d[0]);
//     if ( travel > d[0] ) {
//       return 0.0;
//     } else {
//       return 1.0;
//     }
//   } else {
//     return 1.0;
//   }
// }

module.exports.Brain = Brain;
