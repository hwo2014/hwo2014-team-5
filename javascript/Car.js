// HWO - Car object

var Car = function( precision, debug_msg ) {

  this.precision = precision;
  this.debug_msg = debug_msg;

  // car id and dimensions dimensions - do not reset!
  this.name = '';
  this.color = '';
  this.length = 0.0;
  this.width = 0.0;
  this.guideFlag = 0.0;
  this.spawnTime = 0;     // calculate from crash & spawn and set to all cars
  this.tightnessScope = 100;

  this.k = -1; // drag constant
  this.m = -1; // mass of the car
  this.hasPhysics = false; // true when physics are set
  this.isMe = false;  // true when this car object is us.
  this.maxCurveForce = 0.5; // set and refined always when somebody crashes
  this.maxCurveForceAdd = 0; // amount to add on every tick (until we crash in qualify or we get maxAngle close to 60)

  this.resetGameInit();

  this.closestEnemies = {
    'ahead': {
      'keep': -1.0,
      'left': -1.0,
      'right': -1.0
    },
    'behind': {
      'keep': -1.0,
      'left': -1.0,
      'right': -1.0
    }
  };

  this.spawningEnemies = {
    'keep': [],
    'left': [],
    'right': []
  };
  
}

Car.prototype.resetControl = function() {
  // PRIORITY 1
  // when swicthLane evaluates to true: sends switch on next send & resets this.
  this.switchLane = false;        // USE: 'Left' or 'Right' or false
  this.goingToSwitch = false;     // true when we have active switch command in
  this.goingToLane = 0;           // target lane index which we try to switch to
  this.switchStarted = false;     // true when we start to switch (switch success)
  this.inSwitch = false;          // true when car is currently changing lane.
  this.switchFinished = false;    // true right after switch is over.

  // PRIORITY 2
  // when set to true, sends turbo on next possible send ( Switchlane first, then turbo ).
  this.useTurbo = false;

  // PRIORITY 3
  this.throttle = 1.0;
}
Car.prototype.resetGameInit = function() {
  this.spareTurboAvailable = false; // DO NOT MOVE THESE!
  this.spareTurbo = {}; // DO NOT MOVE THESE
  this.resetSpawn();
  this.resetCarPositions();
  this.resetLapStatistics();
}

Car.prototype.resetCrash = function() {
  this.spareTurboAvailable = false; // DO NOT MOVE THESE
  this.spareTurbo = {};  // DO NOT MOVE THESE
  this.resetTurbo();
  this.resetControl();
  this.resetDynamics();
}

Car.prototype.resetSpawn = function() {
  this.spareTurboAvailable = false; // DO NOT MOVE THESE
  this.spareTurbo = {}; // DO NOT MOVE THESE
  this.resetTurbo();
  this.resetControl();
  this.resetCrashData();
  this.resetDynamics();
}

Car.prototype.resetCarPositions = function() {
  // data from carPositions
  this.angle = 0.0;
  this.maxAngle = 0.0;
  this.pieceIndex = 0.0;
  this.inPieceDistance = 0.0;
  this.currentLane = 0;
  this.targetLane = 0;
  this.lap = 0;
  this.pieceAngle = 0;
  this.pieceRadius = 0;

  // previous tick values
  this.p_angle = 0.0;
  this.p_pieceIndex = 0.0;
  this.p_inPieceDistance = 0.0;
  this.p_currentLane = 0;
  this.p_targetLane = 0;
  this.p_lap = 0;
  this.p_pieceAngle = 0;
  this.p_pieceRadius = 0;

  // left and right lanes ( compared to currentLane )
  this.leftLanes = [];
  this.rightLanes = [];

  // next disturbance
  this.nextDisturbance = [0, 0, 0, 0, 0];
}

Car.prototype.resetTurbo = function() {
  this.inTurbo = false;
  this.turboAvailable = false;
  this.turbo = {
    msg: 'Imposssibbrruuuuu!!',
    duration: 0,
    left: 0,
    factor: 0.0
  }
  if ( this.spareTurboAvailable ) {
    this.turboAvailable = true;
    this.turbo.duration = this.spareTurbo.duration;
    this.turbo.factor = this.spareTurbo.factor;
    this.spareTurboAvailable = false;
    this.spareTurbo = {};
  }
}

Car.prototype.resetCrashData = function() {
  // crash and spawn
  this.crashed = false;   // true when car is out of track and crashed
  this.crash = {
    crashTick: 0,         // set when car crashes
    spawnTick: 0,         // set when car spawns
    spawnsIn : 0,         // spawns back in x ticks ( we must calc spawntime )
    pieceIndex : 0,       // piece index to spawn in
    inPieceDistance : 0   // distance from piece start to spawn in
  }
}

Car.prototype.resetLapStatistics = function() {
  // lapFinishedStats (we might want to use these just to compare
  // our own stats -- e.g. who is leading, estimate when a car will
  // finish etc. )
  this.lapTimes = [];
  this.raceTime = {
    laps: 0,
    ticks: 0,
    millis: 0
  };
  this.ranking = {
    overall: 0,
    fastestLap: 0,
    fastestTicks: 0,
    fastestMillis: 0
  };
}

Car.prototype.resetDynamics = function() {
  // calculated
  this.vel = 0.0;
  this.acc = 0.0;
  this.jerk = 0.0;
  this.aVel = 0.0;
  this.aAcc = 0.0;
  this.aJerk = 0.0;

  this.pp_vel = 0.0;
  this.p_vel = 0.0;
  this.p_acc = 0.0;
  this.p_jerk = 0.0;
  this.p_aVel = 0.0;
  this.p_aAcc = 0.0;
  this.p_aJerk = 0.0;

  this.tick = -1;         // +1 only when vel > 0, reset to -1 when crash.
  this.isMoving = false;  // true only when vel > 0.
}

Car.prototype.turboAppeared = function( d ) {
  // Turbo is reset after spawn, turbos dont' stack and MOST OF ALL - while inCrash, ignore turbo for that car!
  // If turbo appears while inTurbo, it is available to use right after this turbo ends.
  if ( !this.crashed && !this.inTurbo ) {
    this.turbo.factor = d.turboFactor;
    this.turbo.duration = d.turboDurationTicks;
    this.turboAvailable = true;
  } else if ( !this.crashed && this.inTurbo ) {
    // we must store this turbo on hold
    this.spareTurbo = {
      factor: d.turboFactor,
      duration: d.turboDurationTicks,
      left: d.turboDurationTicks
    }
    this.spareTurboAvailable = true;
  } else {
    this.resetTurbo();
  }
}

Car.prototype.carPositions = function( d, track ) {
  
  if ( this.crashed ) {
    if ( this.spawnTime ) {
      this.crash.spawnsIn -= 1;
    }
    this.crash.pieceIndex = d.piecePosition.pieceIndex;
    this.crash.inPieceDistance = d.piecePosition.inPieceDistance;
  }

  this.p_angle = this.angle;
  this.angle = d.angle;
  if ( Math.abs(this.angle) > this.maxAngle ) {
    this.maxAngle = Math.abs(this.angle);
  }
  this.p_lap = this.lap;
  this.lap = d.lap;
  this.p_pieceIndex = this.pieceIndex;
  this.pieceIndex = d.piecePosition.pieceIndex;
  this.p_inPieceDistance = this.inPieceDistance;
  this.inPieceDistance = d.piecePosition.inPieceDistance;
  this.p_currentLane = this.currentLane;
  this.currentLane = d.piecePosition.lane.startLaneIndex;
  this.p_targetLane = this.p_targetLane;
  this.targetLane = d.piecePosition.lane.endLaneIndex;
  this.p_pieceAngle = this.pieceAngle;
  this.pieceAngle = track.laneLengths[ this.pieceIndex ][ this.currentLane ].angle;
  this.p_pieceRadius = this.pieceRadius;
  this.pieceRadius = track.laneLengths[ this.pieceIndex ][ this.currentLane ].radius;

  this.leftLanes = track.iLanes.slice( 0, track.iLanes.indexOf( this.currentLane ));
  this.rightLanes = track.iLanes.slice( track.iLanes.indexOf( this.currentLane ) + 1);

  //this.curveTightness = track.getTightness( this.tightnessScope, this.pieceIndex, this.currentLane, this.inPieceDistance )
  this.nextDisturbance = track.getNextDisturbance( this.pieceIndex, this.currentLane, this.inPieceDistance );

  if ( this.inSwitch && this.currentLane == this.targetLane ) {
    this.inSwitch = false;
    this.switchFinished = true;
    this.switchLane = false;
    this.goingToSwitch = false;
  }

  // check if we missed that switch
  var pc = track.laneLengths[this.pieceIndex][this.currentLane];
  if ( this.goingToSwitch && pc.switchPiece && this.inPieceDistance > 0.5*pc.keep && !this.inSwitch ) {
    if ( this.debug_msg ) { console.log( 'missed switch - enabling switch again' ); }
    this.switchLane = false;
    this.goingToSwitch = false;
  }
  
  if ( this.currentLane != this.targetLane ) {
    this.inSwitch = true;
    this.currentLane = this.targetLane;
  } else {
    this.inSwitch = false;
  }

  this.pp_vel = this.p_vel;
  this.p_vel = this.vel;
  this.p_acc = this.acc;
  this.p_jerk = this.jerk;
  this.p_aVel = this.aVel;
  this.p_aAcc = this.aAcc;
  this.p_aJerk = this.aJerk;

  // calculate dynamics - TODO: problems during SHORT pieces & SWITCHES.
  if ( this.p_pieceIndex == this.pieceIndex ) {
    this.vel = this.inPieceDistance - this.p_inPieceDistance;
  } else {
    var prevPiece = track.laneLengths[ this.p_pieceIndex ][ this.p_currentLane ].keep - this.p_inPieceDistance;
    this.vel = prevPiece + this.inPieceDistance;
  }

  this.acc = this.vel - this.p_vel;
  this.jerk = this.acc - this.p_acc;

  this.aVel = this.angle - this.p_angle;
  this.aAcc = this.aVel - this.p_aVel;
  this.aJerk = this.aAcc - this.p_aAcc;

  if ( this.pp_vel > 0 ) {
    this.isMoving = true;
    this.tick += 1;
    if ( !this.hasPhysics && this.isMe ) {
      this.calcPhysics();
      this.hasPhysics = true;
    }
  } else {
    this.isMoving = false;
    this.tick = -1;
  }
}

Car.prototype.terminalVelocity = function( t ) {
  // terminal velocity for this throttle
  if ( this.hasPhysics ) {
    return t / this.k;
  }
  console.log( 'CALLED terminalVelocity IN Car.js WITHOUT PHYSICS!' );
  return 0;
}

Car.prototype.throttleForTerminalVelocity = function( v ) {
  // required throttle for given terminal velocity
  if ( this.hasPhysics ) {
    return v * this.k;
  }
  console.log( 'CALLED throttleForTerminalVelocity IN Car.js WITHOUT PHYSICS!' );
  return 0;
}

Car.prototype.speedInTicks = function( ticks, t, startVel ) {
  // speed in a given amount of ticks if throttle is t and starting velocity is startVel
  if ( this.hasPhysics ) {
    return ( startVel - ( t / this.k ) ) * Math.pow( Math.E, ( ( -this.k * ticks ) / this.m ) ) + ( t / this.k );
  }
  console.log( 'CALLED speedInTicks IN Car.js WITHOUT PHYSICS!' );
  return 0;
}

Car.prototype.ticksNeededForSpeed = function ( v, t, startVel ) {
  // required amount of ticks needed for given speed with given throttle and starting velocity
  if ( this.hasPhysics ) {
    return Math.ceil( ( Math.log( ( v - ( t / this.k ) ) / ( startVel - ( t / this.k ) ) ) * this.m ) / ( -this.k ));
  }
  console.log( 'CALLED ticksNeededForSpeed IN Car.js WITHOUT PHYSICS!' );
  return 0;
}

Car.prototype.distanceTraveledInTicks = function ( t, start, ticks, startVel ) {
  // returns the distance traveled in given ticks relative to start distance (0.0) for relative distance.
  if ( this.hasPhysics ) {
    return ( this.m / this.k ) * ( startVel - ( t / this.k ) ) * ( 1.0 - Math.pow( Math.E, ( ( -this.k * t ) / this.m ) ) ) + ( t / this.k ) * ticks + start;
  }
  console.log( 'CALLED distanceTraveledInTicks IN Car.js WITHOUT PHYSICS!' );
  return 0;
}

Car.prototype.estimateThrottle = function () {
  // returns the estimated throttle for two velocities that are between 1 tick
  // used to estimate enemy throttles, as we always know our own.
  if ( this.hasPhysics ) {
    return ( this.k * ( this.vel * Math.pow( Math.E, ( this.k / this.m ) ) - this.p_vel ) / ( Math.pow( Math.E,  ( this.k / this.m ) ) - 1.0 ) );
  }
  console.log( 'CALLED estimateThrottle IN Car.js WITHOUT PHYSICS!' );
  return 0;
}

Car.prototype.setMaxCurveForce = function ( F ) {
  // call this to set and refine the maximum curve speed
  if ( this.maxCurveForce == 0) {
    this.maxCurveForce = F;
    if ( this.debug_msg ) { console.log( 'New maximum curve force set to: %s', F ); }
    return true;
  } else if ( this.maxCurveForce > F ) {
    if ( this.debug_msg ) { console.log( 'New maximum curve force updated to: %s', F ); }
    this.maxCurveForceAdd = 0;
    this.maxCurveForce = F;
    return true;
  }
  return false;
}

Car.prototype.getMaxCurveSpeed = function ( r ) {
  // returns maximum speed for given curve radius
  this.maxCurveForce += this.maxCurveForceAdd;
  if ( this.maxCurveForce > 0 && r != 0) {
    //console.log( 'curveforce: %s %s, maxSpeed: %s', this.maxCurveForce, r, Math.sqrt(this.maxCurveForce * Math.abs(r)));
    return Math.sqrt( this.maxCurveForce * Math.abs(r) );
  }
  return 100.0;
}

Car.prototype.calcPhysics = function () {
  // calculates k and m
  // 1.0 = current throttle, and we always have 1.0 for first ticks
  this.k = ( this.pp_vel - ( this.p_vel - this.pp_vel ) ) / ( this.pp_vel * this.pp_vel ) * 1.0;
  this.m = 1.0 / ( Math.log( ( this.vel - ( 1.0 / this.k ) ) / ( this.p_vel - ( 1.0 / this.k ) ) ) / ( -this.k ) );
  console.log('pp_vel:%s, p_vel:%s, k:%s, m:%s', this.pp_vel, this.p_vel, this.k, this.m);
}

Car.prototype.gameInit = function ( d ) {
  // gets car information from gameInit message
  this.name = d.id.name;
  this.color = d.id.color;
  this.length = d.dimensions.length;
  this.width = d.dimensions.width;
  this.guideFlag = d.dimensions.guideFlagPosition;
  this.resetGameInit();
}

Car.prototype.getSwitchIndex = function ( s ) {
  // returns the lane index to which we are switching to
  if ( s == 'Left' ) {
    if ( this.leftLanes.length > 0 ) {
      return this.leftLanes[ this.leftLanes.length - 1 ];
    } 
  } else {
    if ( this.rightLanes.length > 0 ) {
      return this.rightLanes[0];
    }
  }
  return this.currentLane;
}


Car.prototype.lapFinished = function ( d ) {
  // updates info on lapfinished
  this.lapTimes.push( d.lapTime );
  this.raceTime.laps = d.raceTime.laps;
  this.raceTime.ticks = d.raceTime.ticks;
  this.raceTime.millis = d.raceTime.millis;
  this.ranking.overall = d.ranking.overall;
  this.ranking.fastestLap = d.ranking.fastestLap;
}

Car.prototype.updateClosestEnemies = function (enemies, track) {

  this.closestEnemies = {
    'ahead': {
      'keep': -1.0,
      'left': -1.0,
      'right': -1.0
    },
    'behind': {
      'keep': -1.0,
      'left': -1.0,
      'right': -1.0
    }
  };

  
  for (var color in enemies) {

    var enemy = enemies[color];

    if (enemy.crashed) continue;

    var isBehind = enemy.pieceIndex < this.pieceIndex 
      || enemy.pieceIndex === this.pieceIndex
      && enemy.inPieceDistance < this.inPieceDistance;

    var d;

    if (isBehind) {
      // Enemy is behind us
      if (enemy.pieceIndex === this.pieceIndex) {
        d = this.inPieceDistance - enemy.inPieceDistance;
      } else {
        d = this.inPieceDistance;
        d += track.getKeepLength(enemy.pieceIndex, this.pieceIndex, enemy.currentLane);
        d -= enemy.inPieceDistance;
      }
    } else {
      // Enemy is ahead of us
      if (enemy.pieceIndex === this.pieceIndex) {
        d = enemy.inPieceDistance - this.inPieceDistance;
      } else {
        d = enemy.inPieceDistance;
        d += track.getKeepLength(this.pieceIndex, enemy.pieceIndex, enemy.currentLane);
        d -= this.inPieceDistance;
      }      
    }

    var dir = isBehind ? 'behind' : 'ahead';
    var lane = null;
    
    if (enemy.currentLane === this.currentLane) {
      // Same lane
      lane = 'keep';
    } else if (enemy.currentLane === this.getSwitchIndex('Left') && this.leftLanes.length > 0) {
      // Left lane
      lane = 'left';
    } else if (enemy.currentLane === this.getSwitchIndex('Right') && this.rightLanes.length > 0) {
      // Right lane
      lane = 'right';
    }

    if (!lane) continue;

    if (this.closestEnemies[dir][lane] === -1 || this.closestEnemies[dir][lane] > d) {
      this.closestEnemies[dir][lane] = d;
    }

  }

  // console.log( 'closest enemies: %s', JSON.stringify(this.closestEnemies) );
  
};

Car.prototype.getClosestEnemies = function() {
  return this.closestEnemies;
};

Car.prototype.updateSpawningEnemies = function(enemies, track) {

  this.spawningEnemies = {
    'keep': [],
    'left': [],
    'right': []
  };  

  for (var color in enemies) {

    var enemy = enemies[color];

    if (!enemy.crashed) continue;

    if (!enemy.crash) continue;

    var lane = null;

    if (enemy.crash.crashLane === this.currentLane) {
      // Same lane
      lane = 'keep';
    } else if (enemy.crash.crashLane === this.getSwitchIndex('Left') && this.leftLanes.length > 0) {
      // Left lane
      lane = 'left';
    } else if (enemy.crash.crashLane === this.getSwitchIndex('Right') && this.rightLanes.length > 0) {
      // Right lane
      lane = 'right';
    }

    if (!lane) continue;

    var d;

    

    if (enemy.crash.pieceIndex === this.pieceIndex) {
      d = enemy.crash.inPieceDistance - this.inPieceDistance;
    } else {
      d = enemy.crash.inPieceDistance;
      d += track.getKeepLength(this.pieceIndex, enemy.crash.pieceIndex, enemy.crash.crashLane);
      d -= this.inPieceDistance;
    }


    this.spawningEnemies[lane].push({
      'spawnsIn': enemy.crash.spawnsIn,
      'dist': d
    });

    if (enemy.crash.spawnsIn > 0) {
      --enemy.crash.spawnsIn;
    }

  }

  //console.log('this.spawningEnemies', JSON.stringify(this.spawningEnemies));

};

Car.prototype.getSpawningEnemies = function() {
  return this.spawningEnemies;
};

exports.Car = Car;
