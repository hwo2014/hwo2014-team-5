/* Coder Coded - HWO BOT RACER - Authors: Juha Rantanen, Joonas Ruuskanen, Pekka Toiminen */

var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2] || 'testserver.helloworldopen.com';
var serverPort = process.argv[3] || 8091;
var botName = process.argv[4] || 'Fuzzy Racer 4';
var botKey = process.argv[5] || 'TSqmXvBn09Z+Hw';

console.log( '\nBOT: %s \nSRV: %s:%s\n', botName, serverHost, serverPort );

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
}

// index.js: CISERVER = true ( alter debug params the way you wish )
var CISERVER = false;

// --| CI SERVER SETTINGS |----------------------------------------------------

var dataDict = { name: botName, key: botKey };
var cisettings = {
  raceDict: { msgType: 'join', data: dataDict },
  debug_msg: true,
  debug_perf: false,
  debug_socket: false,
  debug_tick: false,
  debug_phys: false
};

// --| TRACK TESTING |---------------------------------------------------------

var trackName = 'keimola';
var trackName = 'germany';
var trackName = 'usa';
var trackName = 'suzuka';
//var trackName = 'france';
//var trackName = 'elaeintarha';
//var trackName = 'england';
//var trackName = 'imola';
var password = 'a';
var carCount = 3;

// Select race type -- NOTE: USE 'join' FOR AUTOMATED TEST RUNS!

var raceDict = {};
raceDict = { msgType: 'join', data: dataDict };
var raceDict = { msgType: 'joinRace', data: {botId: dataDict, trackName: trackName, password: password, carCount: carCount }};
//var raceDict = { msgType: 'createRace', data: {botId: dataDict, trackName: trackName, password: password, carCount: carCount }};
//raceDict = { msgType: 'joinRace', data: {botId: dataDict, trackName: trackName, carCount: carCount}};

// --| VARIABLES AND CLASSES |-------------------------------------------------

// true - prints gameTick and car tick
var debug_tick = false;

// true - prints socket messages send/receive
var debug_socket = false;

// true - prints general debug messages
var debug_msg = true;

// true - prints physics
var debug_phys = true;

// true - prints tick and time elapsed
var debug_perf = false;

// precision for calculations ( used globally )
var precision = 10;

// Array used for plotting
var resultData = [];

// Stop collecting data after crash (for now)
var stopDataInCrash = true;

// game tick global
var gameTick = -1;

// game start global
var gameStart = false;

// car objects me = our car. ( enemy cars generated later (key = color) )
var Car = require('./Car.js').Car;
var me = new Car( precision, debug_msg );
me.isMe = true;
var enemies = {};

// track object
var Track = require('./Track.js').Track;
var track = new Track( precision, debug_msg );

// our AI
var Brain = require('./Brain.js').Brain;
var brain = new Brain(precision, debug_msg, debug_phys );

if ( CISERVER ) {
  raceDict = cisettings.raceDict;
  debug_msg = cisettings.debug_msg;
  debug_perf = cisettings.debug_perf;
  debug_socket = cisettings.debug_socket;
  debug_tick = cisettings.debug_tick;
} else {
  // Export some data before exiting
  process.on( 'SIGINT', function() {
    var fs = require('fs');
    var dataJson = JSON.stringify(resultData, null, 2);
    var fileName = "data.json";

    fs.writeFile("./" + fileName, dataJson, function(err) {
      if(err) {
        console.log(err);
      } else {
        console.log("Collected data exported to " + fileName);
      }
      console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
      process.exit();
    });
  });
}


// ==| MAIN CODE |=============================================================

// Connect to server
console.log( 'JOIN: \n%s', JSON.stringify( raceDict ) );
console.log( '----------------------------------------' );
client = net.connect( serverPort, serverHost, function() { return send( raceDict ); });
jsonStream = client.pipe( JSONStream.parse() );

// --| GAME LOOP |-------------------------------------------------------------

jsonStream.on( 'data', function( data ) {

  var perfStart = Date.now();
  if ( debug_socket ) { console.log('RECEIVED: %s', JSON.stringify(data)); }
  if ( data.hasOwnProperty( 'gameId' )) { gameId = data.gameId; }
  if ( data.hasOwnProperty( 'gameTick' )) { 
    gameTick = data.gameTick;
    // this line starts a new tick round
    if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : start: %s >>>', gameTick, me.tick, perfStart ); }
  }

  if ( !CISERVER ) {
    if ( me.crashed && stopDataInCrash ) {
      // idling in crash and stop data for crash
    } else {
      resultData.push({
        'tick': gameTick,
        'throttle': me.throttle,
        'velocity': me.vel,
        'acceleration': me.acc,
        'jerk': me.jerk,
        'pieceAngle': me.pieceAngle,
        'carAngle': me.angle,
        'aVelocity': me.aVel,
        'aAcceleration': me.aAcc,
        'aJerk': me.aJerk
      });
    }
  }

  // control AI (assure that we have data to process by isMoving - we always start moving with 1.0 throttle!)
  if ( gameStart && me.isMoving && !me.crashed && !me.inTurbo ) {
    brain.think( me, enemies, track );
  }

  // process messages
  // we get carPositions on EVERY tick!

  if ( data.msgType === 'carPositions' ) {

    var positions = data.data;

    // update physics to all cars ( assuming that we have them for us )
    if ( me.hasPhysics ) {
      for ( var enemycolor in enemies ) {
        if ( !enemies[ enemycolor ].hasPhysics ) {
          enemies[ enemycolor ].k = me.k;
          enemies[ enemycolor ].m = me.m;
          enemies[ enemycolor ].hasPhysics = true;
        }
      }
    }

    // update our car and enemy cars
    for ( var i = 0; i < positions.length; i++ ) {
      var color = positions[i].id.color;
      if ( color == me.color ) {
        if ( !me.crashed ) {
          me.carPositions( positions[i], track );
        }
      } else {
        if ( enemies.hasOwnProperty(color) ) {
          if ( !enemies[color].crashed ) {
            enemies[color].carPositions( positions[i], track );
            if ( enemies[color].hasPhysics ) {
              enemies[color].throttle = enemies[color].estimateThrottle();
            }
          }
        }
      }
    }

    me.updateClosestEnemies(enemies, track);
    me.updateSpawningEnemies(enemies, track);
    
    if ( me.crashed ) {
      // while crashed, send ping
      if ( debug_socket ) { console.log( 'SEND [ping]' ); }
      send({
        msgType: "ping",
        data: {}
      });
      if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
      if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }

    } else if ( me.switchLane ) {
      if ( debug_socket ) { console.log( 'SEND [SWITCHLANE]: %s', me.switchLane ); }
      if ( debug_msg ) { console.log( 'SEND SWITCHLANE: %s', me.switchLane ); }
      send({
        msgType: "switchLane",
        data: me.switchLane,
        gameTick: gameTick
      });
      if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
      if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }

      me.goingToLane = me.getSwitchIndex( me.switchLane ); // set target lane index
      me.switchLane = false;  // reset so we send only one switch command at a time
      me.goingToSwitch = true;

    } else if ( me.turboAvailable && me.useTurbo ) {
      send({
        msgType: "turbo",
        data: me.turbo.msg,
        gameTick: gameTick
      });
      if ( debug_msg ) { console.log( 'turbo launched!' ); }
      if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
      if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }

      me.inTurbo = true;
      me.useTurbo = false;
      me.turboAvailabe = false;

    } else {
    
      if ( debug_socket ) { console.log( 'SEND [THROTTLE]: %s', me.throttle ); }
    
      send({
        msgType: "throttle",
        data: me.throttle,
        gameTick: gameTick
      });
      if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
      if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }

    }
  
  } else if ( data.msgType === 'gameStart' ) {
      if ( debug_msg ) { console.log( 'gameStart - replying with throttle: %s', me.throttle ); }
      send({
        msgType: "throttle",
        data: me.throttle,
        gameTick: gameTick
      });
      if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
      if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }
      gameStart = true;

  } else {
   
    if ( data.msgType === 'yourCar' ) {

      me.color = data.data.color;
      me.name = data.data.name;

      if ( debug_msg ) { console.log( 'yourCar [name: %s], [color: %s]', me.name, me.color ); }

    } else if ( data.msgType === 'gameInit' ) {
  
      // create enemies
      var raceCars = data.data.race.cars;
      for ( var i = 0; i < raceCars.length; i++ ) {
        var color = raceCars[i].id.color;
        if ( color == me.color ) {
          me.gameInit( raceCars[i] );
          if ( debug_msg ) { console.log( 'gameInit me [%s]: %s', color, JSON.stringify( raceCars[i] )); }
        } else {
          if ( !enemies.hasOwnProperty(color) ) {
            brain.noEnemies = false;
            enemies[color] = new Car( precision );
          }
          if ( debug_msg ) { console.log( 'gameInit enemy [%s]: %s', color, JSON.stringify( raceCars[i] )); }
          enemies[color].gameInit( raceCars[i] );
        }
      }

      track.gameInit( data.data.race );

    } else if ( data.msgType === 'join' ) {
      if ( debug_msg ) { console.log( 'join[reply] - OK' ); }

    } else if ( data.msgType === 'joinRace' ) {
      if ( debug_msg ) { console.log( 'joinRace[reply] - OK' ); }
    
    } else if ( data.msgType === 'gameEnd' ) {
      // TODO: Log the data that we receive...
      if ( debug_msg ) { console.log( 'gameEnd - switching from qualify -> race -> end' ); }
      gameStart = false;
      brain.initRace = true;
    
    } else if ( data.msgType === 'tournamentEnd' ) {
      console.log( 'Tournament ended' );
      // TODO: POSSIBLY NEED TO DO SEVERAL TOURNAMENTS???

    } else if ( data.msgType === 'lapFinished' ) {
      var color = data.data.car.color;
      if ( color == me.color ) {
        me.lapFinished( data.data );
        console.log('Lap Finished: %s', JSON.stringify(data.data));
      } else {
        enemies[color].lapFinished( data.data );
      }
      if ( debug_msg ) { console.log( 'lapFinished [%s]: %s ticks, %s millis', color, data.data.lapTime.ticks, data.data.lapTime.millis ); }

    } else if ( data.msgType === 'crash' ) {
      var color = data.data.color;
      var crashtick = data.gameTick;

      if ( color == me.color ) {
        // it was us
        me.crashed = true;
        me.crash.crashTick = crashtick;
        me.crash.pieceIndex = me.pieceIndex;
        me.crash.inPieceDistance = me.inPieceDistance;
        me.crash.crashLane = me.currentLane;
        if ( me.spawnTime ) {
          // we know spawnTime
          me.crash.spawnsIn = me.spawnTime;
          me.crash.spawnTick = me.spawnTime + crashtick;
        }
        if ( debug_msg ) { console.log( 'I CRASHED! - %s', JSON.stringify( me.crash ) ); }
        if ( me.pieceRadius != 0) {
          if ( me.setMaxCurveForce( me.pp_vel * me.pp_vel / me.pieceRadius ) ) {
            for ( var enemycolor in enemies ) {
              enemies[enemycolor].setMaxCurveForce( me.pp_vel * me.pp_vel / me.pieceRadius );
            }
          }
        }
        me.resetCrash();

      } else {
        // it was enemy
        enemies[color].crashed = true;
        enemies[color].crash.crashTick = crashtick;
        enemies[color].crash.pieceIndex = enemies[color].pieceIndex;
        enemies[color].crash.inPieceDistance = enemies[color].inPieceDistance;
        enemies[color].crash.crashLane = enemies[color].currentLane;
        if ( enemies[color].spawnTime ) {
          // we know spawntime
          enemies[color].crash.spawnsIn = enemies[color].spawnTime;
          enemies[color].crash.spawnTick = enemies[color].spawnTime + crashtick;
        }
        if ( debug_msg ) { console.log( 'ENEMY CRASHED! - %s', JSON.stringify( enemies[color].crash ) ); }
        if ( enemies[color].pieceRadius != 0 ) {
          if ( enemies[color].setMaxCurveForce( enemies[color].pp_vel * enemies[color].pp_vel / enemies[color].pieceRadius ) ) {
            for ( var enemycolor in enemies ) {
              enemies[enemycolor].setMaxCurveForce( enemies[color].pp_vel * enemies[color].pp_vel / enemies[color].pieceRadius );
            }
            me.setMaxCurveForce( enemies[color].pp_vel * enemies[color].pp_vel / enemies[color].pieceRadius );
          }
        }
        enemies[color].resetCrash();
      }

    } else if ( data.msgType === 'spawn' ) {
      var color = data.data.color;
      var spawntick = data.gameTick;
      var spawntime = 0;

      if ( color == me.color ) {
        // it was us
        me.crashed = false;
        me.crash.spawnTick = spawntick;
        spawntime = me.crash.spawnTick - me.crash.crashTick;
        me.throttle = 1.0;
        if ( debug_msg ) { console.log( 'I SPAWNED - replying with throttle %s! - %s', me.throttle, JSON.stringify( me.crash ) ); }
        send({
          msgType: "throttle",
          data: me.throttle,
          gameTick: gameTick
        });
        if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
        if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }
        me.resetSpawn();
      } else {
        // it was enemy
        enemies[color].crashed = false;
        enemies[color].crash.spawnTick = spawntick;
        spawntime = enemies[color].crash.spawnTick - enemies[color].crash.crashTick;
        if ( debug_msg ) { console.log( 'ENEMY SPAWNED! - %s', JSON.stringify( enemies[color].crash ) ); }      
        enemies[color].resetSpawn();
      }

      if ( spawntime ) {
        me.spawnTime = spawntime;
        for ( var enemycolor in enemies ) {
          enemies[enemycolor].spawnTime = spawntime;
        } 
      }

    } else if ( data.msgType === 'dnf' ) {
      // TODO: disqualified alert (reason etc.)
      var color = data.data.car.color;
      if ( color == me.color ) {
        if ( debug_msg ) { console.log( 'WE GOT DISQUALIFIED!! Reason: %s', data.data.reason ); }
      } else {
        // remove disqualified enemy car
        if ( debug_msg ) { console.log( 'ONE DOWN [%s]! Reason: %s', color, data.data.reason ); }
        delete( enemies[color] );
      }
    
    } else if ( data.msgType === 'turboAvailable' ) {
      if ( debug_msg ) { console.log( 'Turbo Available: %s', JSON.stringify( data )); }
      me.turboAppeared( data.data );
      for ( var enemycolor in enemies ) {
        enemies[enemycolor].turboAppeared( data.data );
      }       
    } else if ( data.msgType === 'turboStart' ) {
      var color = data.data.color;
      if ( debug_msg ) { console.log( 'Turbo Start: %s', color ); }
      if ( color == me.color ) {
        me.inTurbo = true;
      } else {
        enemies[color].inTurbo = true;
      }
    
    } else if ( data.msgType === 'turboEnd' ) {
      var color = data.data.color;
      if ( debug_msg ) { console.log( 'Turbo End: %s', color ); }
      if ( color == me.color ) {
        me.resetTurbo();
      } else {
        enemies[color].resetTurbo();
      }  

    } else if ( data.msgType === 'finish' ) {
      // TODO: update finished cars
      console.log(' finish ');
      console.log( JSON.stringify(data) );

    } else {
      console.log( '\nALERT - UNCAUGHT MESSAGE !!!' );
      console.log( JSON.stringify(data) );
      console.log( 'END OF MESSAGE\n' );
    }

    if ( debug_socket ) { console.log( 'SEND [ping]' ); }
    send({
      msgType: "ping",
      data: {}
    });
    if ( debug_tick ) { console.log( '\nTICK [game / me]: %s / %s : elapsed: %s/60', gameTick, me.tick, Date.now() - perfStart ); }
    if ( debug_perf ) { console.log( '%s/%s - %s ms', gameTick, me.tick, Date.now() - perfStart ); }

  }

});

jsonStream.on('error', function() {
  return console.log("disconnected due to stream error - not our code");
});


// function updateCarPositions( positions ) {
//   // Updates:
//   // myCar
//   // otherCars
//   // carAngle
//   // pieceAngle
//   // inSwitch
//   // currentLane
//   // leftLanes
//   // rightLanes
//   // pieceIndex
//   // inPieceDistance
//   // lap

//   p_pieceIndex = pieceIndex;
//   p_inPieceDistance = inPieceDistance;
//   p_currentLane = currentLane;
//   p_carAngle = carAngle;

//   for ( var i = 0; i < positions.length; i++ ) {
//     if ( positions[i].id.color == botColor ) {
//       myCar = positions.pop(i);
//       otherCars = positions;
//       break;
//     }
//   }

//   carAngle = myCar.angle;
  
//   // Calculate lanes to left & right, and whether we're in switch atm.
//   inSwitch = false;
//   currentLane = myCar.piecePosition.lane.startLaneIndex;
//   if (currentLane != myCar.piecePosition.lane.endLaneIndex) {
//     inSwitch = true;
//   }

//   leftLanes = iLanes.slice(0,iLanes.indexOf(currentLane));
//   rightLanes = iLanes.slice(iLanes.indexOf(currentLane)+1);

//   pieceIndex = myCar.piecePosition.pieceIndex;
//   inPieceDistance = myCar.piecePosition.inPieceDistance;

//   lap = myCar.piecePosition.lap;

//   pieceAngle = pieces[pieceIndex].hasOwnProperty('angle') ? pieces[pieceIndex].angle : 0;

//   // these can be optimized for race (as qualify is the same track as race)
//   if (tickCount > 0) {
//     p_velocity = velocity;
//     velocity = calcVelocity();
//     p_aVelocity = aVelocity;
//     aVelocity = carAngle - p_carAngle;
//   }

//   if (tickCount > 1) {
//     p_acceleration = acceleration;
//     acceleration = velocity - p_velocity;

//     p_aAcceleration = aAcceleration;
//     aAcceleration = aVelocity - p_aVelocity;
//   }

//   if (tickCount > 2) {
//     jerk = acceleration - p_acceleration;
//     aJerk = aAcceleration - p_aAcceleration;
//   }

//   // console.log ('current v: %s, a: %s, jerk: %s, angle: %s, av: %s, aa: %s, aj: %s', velocity, acceleration, jerk, carAngle, aVelocity, aAcceleration, aJerk);

// }

// function calcVelocity() {
//   // calculates traveled distance / tick
//   // problematic switches during...

//   if (p_pieceIndex == pieceIndex) {
//     return inPieceDistance - p_inPieceDistance;
//   } else {
//     return (laneLengths[p_pieceIndex][p_currentLane].keep - p_inPieceDistance) + inPieceDistance;
//   }

// }

// function preprocessRoutes( trackData ) {
//   // Calculates:
//   // lanes  (index, distanceFromCenter)
//   // iLanes (just the indexes)
//   // numLanes
//   // pieces
//   // lapModulo
//   // switchPieceIndexes
//   // laneLengths
//   // switchPieceModulo

//   //console.log(JSON.stringify(trackData));

//   // WE MUST ALSO TAKE INTO ACCOUNT THE LANE WIDTHS AT SOME POINT!

//   var i = 0; // JSHINT keeps complaining about i being already defined
//              // so define it here

//   // ensure that our lanes are sorted from left to right
//   lanes = trackData.track.lanes;
//   lanes.sort( function(a, b) { return a.distanceFromCenter - b.distanceFromCenter; });
//   numLanes = lanes.length;

//   for (i = 0; i < lanes.length; i++) {
//     iLanes.push(lanes[i].index);    
//   }

//   pieces = trackData.track.pieces;
//   lapModulo = pieces.length;

//   for (i = 0; i < pieces.length; i++) {
//     if ( pieces[i].hasOwnProperty('switch') ) {
//       if ( pieces[i].switch ) {
//         switchPieceIndexes.push(i);  
//       }
//     }
//     laneLengths.push( calculateLaneLengths( i ) );
//   }

//   swicthPieceModulo = switchPieceIndexes.length;

//   console.log('--| Preprocess Routes |--');
//   console.log('numLanes: ' + numLanes);
//   console.log('lanes: ' + JSON.stringify(lanes));
//   console.log('lapModulo: ' + lapModulo);
//   console.log('switchPieceIndexes: ' + switchPieceIndexes);
//   console.log('switchPieceModulo: ' + switchPieceModulo);
//   console.log(JSON.stringify(laneLengths));

// }

// function getNextSwitchIndex( fromPieceIndex ) {
//   // returns index to switchPieceIndexes list
//   // it points to next switch piece from given piece index

//   var i = fromPieceIndex;
//   var nextSwitchIndex = switchPieceIndexes.indexOf(i);
//   while (nextSwitchIndex < 0) {
//     i = (i + 1) % lapModulo;
//     nextSwitchIndex = switchPieceIndexes.indexOf(i);
//   }
//   return nextSwitchIndex;
// }

// function getKeepDistance( targetIndex ) {

//   // no switches in between, just distance to targetIndex
//   // when current lane is kept. We return negative distances
//   // if we're already driving on the targetIndex

//   var i = pieceIndex;
//   var distanceToPiece = 0;

//   if (i != targetIndex) {
//     distanceToPiece += laneLengths[i][currentLane].keep - inPieceDistance;
//     while ( i != targetIndex - 1) {
//       distanceToPiece += laneLengths[i][currentLane].keep;
//       i = (i + 1) % lapModulo;
//     }
//   } else {
//     distanceToPiece -= inPieceDistance;
//   }

//   return distanceToPiece;
// }


// function getKeepLength( startIndex, endIndex, lane ) {
//   // calculates length from startIndex (including it)
//   // to endIndex (excluding it)

//   var i = startIndex;
//   var keepLength = 0;

//   while ( i != endIndex) {
//     keepLength = keepLength + laneLengths[i][lane].keep;
//     i = (i + 1) % lapModulo;
//   }

//   return keepLength;
// }

// function getSwitchLengths( startSwitch, endSwitch ) {

//   // dir: 0 = keep, -1 = left, +1 = right

//   var r = [];
  
//   r.push({
//     dir: 0,
//     len: getKeepLength( startSwitch, endSwitch, currentLane )
//   });
  
//   if (leftLanes.length > 0) {
//     r.push({
//       dir: -1,
//       len: getKeepLength( startSwitch+1, endSwitch, leftLanes[leftLanes.length-1] ) + laneLengths[startSwitch][currentLane].left 
//     });
//   }

//   if (rightLanes.length > 0) {
//     r.push({
//       dir: 1,
//       len: getKeepLength( startSwitch+1, endSwitch, rightLanes[0] ) + laneLengths[startSwitch][currentLane].right
//     });
//   }

//   return r;  
// }


// function projectPieceProbe( distance ) {
//   // here I practice some probing...
//   // this function returns the piece information that the
//   // probe is in, and the angle if it is a curved piece
//   // pieceIndex is our current piece
//   // inPieceDistance is our traveled distance on that piece
//   // check first how many pieces we must travel
//   var d = distance;
//   var p = pieceIndex;

//   // first take off this piece
//   d -= laneLengths[p][currentLane].keep - inPieceDistance;
//   if (d <= 0) {
//     // we are on the current piece
//     return laneLengths[p][currentLane];
//   }
//   while (d > 0) {
//     p = (p + 1) % lapModulo;
//     d -= laneLengths[p][currentLane].keep;
//   }
//   return laneLengths[p][currentLane];
// }

// // function projectProbe( distance ) {
// //   // this function returns the probe vector from our current location
// //   // it returns the length of the tangential component, length of the
// //   // normal component and the angle between those two.
// //   var d = distance;
// //   var p = pieceIndex;
  
// //   var dy = 0;
// //   var dx = 0;
// //   var theta = 0;

// //   while (d > 0) {
// //     if (p == pieceIndex) {
// //       d -= (laneLengths[p][currentLane].keep - inPieceDistance)
// //     }
// //   }

// function routeAdvisor() {

//   // Task 1: Give always shortest route, distancewise.
//   // pieceIndex, inPieceDistance, switchPieceIndexes

//   var nextSwitchIndex = getNextSwitchIndex( pieceIndex );
//   var nextSwitchPiece = switchPieceIndexes[ nextSwitchIndex ];
  
//   var nextSwitchIndex2 = getNextSwitchIndex( (nextSwitchPiece + 1) % lapModulo );
//   var nextSwitchPiece2 = switchPieceIndexes[ nextSwitchIndex2 ];

//   var distanceToSwitchPiece = getKeepDistance( nextSwitchPiece );

//   var shortestLane = getSwitchLengths( nextSwitchPiece, nextSwitchPiece2 );
//   shortestLane.sort( function(a, b) { return a.len - b.len; });

//   var pieceProbe = projectPieceProbe( velocity * 10 );

//   // now we have distanceToSwitchPiece and shortestLane...

//   // console.log(switchPieceIndexes);
//   // console.log(shortestLane);
//   // console.log('Next Switch Index: ' + nextSwitchPiece);
//   // console.log('Distance To Switch Piece: ' + distanceToSwitchPiece);



//   // i = pieceIndex;
//   // var distanceToSwitchPiece = 0;

//   // if (i != nextSwitchIndex) {
//   //   distanceToSwitchPiece += laneLengths[pieceIndex][currentLane].keep - inPieceDistance;
//   //   while (i != nextSwitchPiece) {
//   //     i = (i + 1) % lapModulo;
//   //     distanceToSwitchPiece += laneLengths[pieceIndex][currentLane].keep;
//   //   }
//   // } else {
//   //   distanceToSwitchPiece -= inPieceDistance;
//   // }


//   // calculate suggested route from next switch (naive shortest one)
//   // we can only switch from [left + right] on this lane

//   return {
//     nextSwitchIndex: nextSwitchIndex,
//     distanceToSwitchPiece: distanceToSwitchPiece,
//     shortestLane: shortestLane,
//     pieceProbe: pieceProbe
//   };

// }

// function calculateLaneLengths( pieceIndex ) {
//   // returns all lane lengths of given pieceIndex
//   var r = [];
//   var p = pieces[pieceIndex];
//   var i = 0;

//   var c = 0, cl = 0, cr = 0; // Curve length
//   var sll = 0;
//   var slr = 0;
//   var ldl = 0, ldr = 0; // Lane distance

//   // straight piece with or without switch
//   if ( p.hasOwnProperty('length') ) {
//     for ( i = 0; i < numLanes; i++ ) {
//       if ( p.hasOwnProperty('switch') ) {   

//         if (lanes[i-1]) {
//           ldl = Math.abs(lanes[i-1].distanceFromCenter - lanes[i].distanceFromCenter);
//           sll = Math.sqrt(Math.pow(ldl, 2) + Math.pow(p.length, 2));
//         }

//         if (lanes[i+1]) {
//           ldr = Math.abs(lanes[i+1].distanceFromCenter - lanes[i].distanceFromCenter);
//           slr = Math.sqrt(Math.pow(ldr, 2) + Math.pow(p.length, 2));
//         }

//         if (i === 0) {
//           r.push({ angle: 0, switchPiece: true, keep: p.length, right: slr });
//         } else if (i == (numLanes - 1)) {
//           r.push({ angle: 0, switchPiece: true, keep: p.length, left: sll });
//         } else {
//           r.push({ angle: 0, switchPiece: true, keep: p.length, left: sll, right: slr });
//         }
//       } else {
//         r.push({ angle: 0, switchPiece: false, keep: p.length });
//       }   
//     }
//   } else {
//     // curved piece with or without switch
//     var radius = 0, rl = 0, rr = 0;
//     for ( i = 0; i < numLanes; i++ ) {

//       if (p.angle > 0) {
//         radius = p.radius - lanes[i].distanceFromCenter;

//       } else {
//         radius = p.radius + lanes[i].distanceFromCenter;  
//       }
//       c = (Math.abs(p.angle)*DEGRA) * radius;

//       if ( p.hasOwnProperty('switch') ) {

//         if (lanes[i-1]) {
//           ldl = Math.abs(lanes[i-1].distanceFromCenter - lanes[i].distanceFromCenter);
//         }

//         if (lanes[i+1]) {
//           ldr = Math.abs(lanes[i+1].distanceFromCenter - lanes[i].distanceFromCenter);
//         }

//         if (p.angle > 0) {
//           rl = radius - ldl;
//           rr = radius - ldr;

//         } else {
//           rl = radius + ldl;
//           rr = radius + ldr;
//         }

//         cl = (Math.abs(p.angle)*DEGRA) * rl;
//         cr = (Math.abs(p.angle)*DEGRA) * rr;
        
//         sll = (c + cl) / 2;
//         slr = (c + cr) / 2;

//         if (i === 0) {
//           r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, right: slr });
//         } else if (i == (numLanes - 1)) {
//           r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, left: sll, right: slr });
//         } else {
//           r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, left: sll, right: slr });
//         }
//       } else {
//         r.push({ angle: p.angle, radius: radius, switchPiece: false, keep: c });
//       }
//     }
//   }
//   return r;
// }

// function naiveAI(data) {

//   var prediction;
  
//   if (runOnce) {
//     tC.dbg_vCurve(0, 20, 1.0);
//     tC.dbg_bCurve(0, 200, 0.5);
//     runOnce = false;
//   }

//   var ctrl = {
//     switchLane: false,
//     targetLane: 'Left',
//     throttle: tC.setThrottle( 0.5, velocity )
//   };

//   if (velocity > 0 && noVelocity) {
//     tickCount = 0;
//     noVelocity = false;
//     console.log('\n *** we are moving! *** \n');
//     // TODO: set on crash to true!
//   }

//   if (!noVelocity) {
//     velocity = velocity.toFixed( precision );
//     prediction = tC.ticks2vel( velocity, ctrl.throttle, 0 );
//     //console.log(prediction);
//     console.log('gameTick: %s, acc:%s, vel: %s, error: %s', tickCount, acceleration, velocity, prediction-velocity);
//   }

//   if (tickCount == 50) {
//     ctrl.throttle = tC.setThrottle( 0.2, prediction );
//   } 

//   if (tickCount == 105) {
//     ctrl.throttle = tC.setThrottle( 0.5, prediction );
//   }

//   if (tickCount == 200) {
//     ctrl.throttle = tC.setThrottle( 0.2, prediction );
//   }

//   return ctrl;

//}


