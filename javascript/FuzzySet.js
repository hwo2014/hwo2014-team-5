// HWO - Fuzzy class for Coder Coded racer

var FuzzySet = function( d ) {
  this.name = d.name;
  this.crisp = d.crisp;
  this.crispMax = d.crispMax;  // basically for shelf functions and auto-adjustments
  this.crispMin = d.crispMin;  // same as above
  this.names = d.names;
  this.sets = d.sets;
  this.result = d.result || 0.0;
  this.norm = d.norm || 'prob';
  this.conorm = d.conorm || 'prob';
  this.setNormFunction(this.norm)
  this.setConormFunction(this.conorm)
  if ( this.sets.length != this.names.length ) {
    console.log( 'ERROR IN FUZZYSET %s SET AND NAMES LENGTH DO NOT MATCH!', this.name );
  }
  for ( var i = 0; i < this.sets.length; i++ ) {
    if ( this.sets[i].length == 2 && ( this.sets[i][1] > 1.0 || this.sets[i][1] < 0.0 ) ) {
      console.log( 'ERROR IN FUZZYSET %s SINGLETON VALUE IS NOT BETWEEN 0.0 AND 1.0!', this.name );
    }
  }
};
FuzzySet.prototype.setNormFunction = function (norm) {
  // sets the norm for disjunction
  switch (norm) {
    case 'prob':
      // Probabilistic sum
      this.orFunc = function (a,b) { return a+b-a*b }; break
    case 'bound':
      // Bounded sum (dual to Lukasiewicz t-norm)
      this.orFunc = function (a,b) { return Math.min(a+b, 1) }; break
    case 'nilpotent':
      this.orFunc = function (a,b) {
        if (a+b < 1) return Math.max(a,b)
        return 1.0
      }
      break
    case 'max':
      // Maximum t-conorm
      this.orFunc = function(a,b) { return Math.max(a, b) }; break
    case 'einstein':

 
  }
}

FuzzySet.prototype.setConormFunction = function (conorm) {
  // sets the t-conorm for conjunction. For more information about t-conorms
  // visit: http://http://en.wikipedia.org/wiki/T-norm#Examples_of_t-conorms
  switch (conorm) {
    case 'max':
      // Maximum t-conorm
    case 'prob':
      // probabilistic sum ( union of independent events )
      this.andFunc = function (a,b) { return a*b }; break
    case 'bound':
      // Bounded sum
    case 'drastic':
      // Drastic t-conorm
    case 'nilpotent':
      // nilpotent maximum
    case 'einstein':
      // Einstein sum ( compare to the velocity-addition formula under spec. relativity )
      // Dual to one of the hamacher t-norms
      this.andFunc = function(a,b) { return (a+b)/(1+a*b) }; break
  }
}

FuzzySet.prototype.setNorm = function (norm) {
  switch (norm) {
    case 'prob':
     
      this.andFunc = function (a,b) { return a*b }
      break
    case 'bound':
           this.andFunc = function (a,b) { return Math.max(0, a+b-1) }
      break
    case 'nilpotent':
      this.orFunc = function (a,b) {
        if (a+b < 1) return Math.max(a,b)
        return 1.0
      }
      this.andFunc = function (a,b) {}
      break
    case 'einstein':
      // 
    case 'max':
      this.andFunc = function(a,b) { return Math.min(a, b) }      
    default:
      // Defaults to Maximum t-conorm
      this.orFunc = function(a,b) { return Math.max(a, b) }
      this.andFunc = function(a,b) { return Math.min(a, b) }
  }
}

FuzzySet.prototype.IS = function(v) {
  // inputs string
  var result = 0;
  if (typeof(v) !== 'string') {
    for (var i = 0; i < v.length; i++) {
      result += this.valueFor(v[i]);
    }
  } else {
    result = this.valueFor(v);
  }
  return this.newSet(result)
};

FuzzySet.prototype.NOT = function(v) {
  return this.newSet(1.0 - this.valueFor(v))
};

FuzzySet.prototype.OR = function(s) {
  // inputs another fuzzy set, store result to that set
  return this.newSet(this.orFunc(this.result, set.result))
};

FuzzySet.prototype.AND = function(s) {
  return this.newSet(this.andFunc(this.result, s.result))
};

FuzzySet.prototype._ = function( v ) {
  // returns the maximum value of given variable v in this set
  var n = this.names.indexOf( v );
  if ( n < 0 ) {
    console.log( 'ERROR IN FUZZY SENTENCES %s IS NOT DEFINED IN %s AT %s!', v, this.names, this.name );
  }
  var i = this.sets[ n ];
  if ( i.length == 1 ) {
    // singleton with value 1
    this.result = 1
  } else if ( i.length == 2 ) {
    // singleton with value specified in i[1]
    this.result = i[1]
  } else if ( i.length == 3 ) {
    if ( i[0] == i[1] ) {
      // left shelf
      this.result = this.crispMin;
    } else if ( i[1] == i[2] ) {
      // right shelf
      this.result = this.crispMax;
    } else {
      // triangle
      this.result = i[1];
    }
  } else if ( i.length == 4 ) {
    // trapezoid
    this.result = i[2];
  } else {
    console.log ( 'UNKNOWN SET ELEMENT TYPE IN _ (not a shelf, nor a triangle, nor a trapezoid: %s', i);
    this.result = 0;
  }

  return this;
}
FuzzySet.prototype.HIT = function( v ) {
  // sets max prob for this value
  if (this.result > 0) {
    console.log('HIT');
    return( [ v, this.result, true ]);
  } else {
    return( [ 0, 1, false ]);
  }
};

FuzzySet.prototype.ACT = function( s ) {
  //console.log("ACT: this:%s, s:%s", this.result, s.result);
  return ( [ this.result, s.result, false ] );
};

FuzzySet.prototype.valueFor = function( v ) {
  // do the fuzzy math for set variable
  var r = 0;
  var n = this.names.indexOf( v );
  if ( n < 0 ) {
    console.log( 'ERROR IN FUZZY SENTENCES %s IS NOT DEFINED IN %s AT %s!', v, this.names, this.name );
  }
  var i = this.sets[ n ];

  if ( i.length == 1 ) {
    // singleton with value 1
    if ( this.crisp == i[0] ) {
      r = 1;
    } else {
      r = 0;
    }
  } else if ( i.length == 2 ) {
    // singleton with pre-defined value
    if ( this.crisp == i[0] ) {
      r = i[1];
    } else {
      r = 0;
    }
  } else if ( i.length == 3 ) {
    if ( i[0] == i[1] ) {
      // left shelf: onwards to i[1] = 1.0, i[1]..i[2] linear. i[2] onwards 0
      if ( this.crisp <= i[0] ) {
        r = 1;
      } else if ( this.crisp >= i[2] ) {
        r = 0;
      } else {
        //r = ( -this.crisp / ( i[2] - i[1] )) + ( i[2] / ( i[2] - i[1] ) );
        // left tilt \
        r = Math.abs( i[2] - this.crisp ) / Math.abs( i[2] - i[1] );
      }

    } else if ( i[2] == i[1] ) {
      // right shelf: i[0] = start i[1] onwards a shelf, i[0]..i[1] linear
      if ( this.crisp <= i[0] ) {
        r = 0;
      } else if ( this.crisp >= i[1] ) {
        r = 1;
      } else {
        //r = ( this.crisp / ( i[1] - i[0] ) ) - ( i[0] / ( i[1] - i[0] ) );
        // right tilt /
        r = Math.abs( this.crisp - i[0] ) / Math.abs( i[1] - i[0] );
      }

    } else {
      // triangle
      if ( this.crisp <= i[0] ) {
        r = 0;
      } else if ( this.crisp >= i[2] ) {
        r = 0;
      } else if ( ( this.crisp > i[0] ) && ( this.crisp <= i[1] ) ) {
        // right tilt /
        r = Math.abs( this.crisp - i[0] ) / Math.abs( i[1] - i[0] );
      } else if ( ( this.crisp > i[1] ) && ( this.crisp < i[2] ) ) {
        // left tilt \
        r = Math.abs( i[2] - this.crisp ) / Math.abs( i[2] - i[1] );
      }
    }
  } else if ( i.length == 4 ) {
    // trapezoid ( 4 values )
    if ( this.crisp <= i[0] ) {
      r = 0;
    } else if ( this.crisp >= i[3] ) {
      r = 0;
    } else if ( ( this.crisp >= i[1] ) && ( this.crisp <= i[2] ) ) {
      r = 1;
    } else if ( ( this.crisp > i[0] ) && ( this.crisp < i[1] ) ) {
      // right tilt /
      r = Math.abs( this.crisp - i[0] ) / Math.abs( i[1] - i[0] );
    } else {
      // left tilt \
      r = Math.abs( i[3] - this.crisp ) / Math.abs( i[3] - i[2] );
    }
  } else {
    console.log ( 'UNKNOWN SET ELEMENT TYPE IN valueFor (not a shelf, nor a triangle, nor a trapezoid: %s', i);
  }
  // TODO: other kind of functions (Gaussian?, exponential?)
  return r;
};

FuzzySet.prototype.newSet = function(result) {
  return new FuzzySet({
    name: this.name
   ,crisp: this.crisp
   ,crispMin: this.crispMin
   ,crispMax: this.crispMax
   ,names: this.names
   ,sets: this.sets
   ,norm: this.norm
   ,conorm: this.conorm
   ,result: result
  });
}

exports.FuzzySet = FuzzySet;