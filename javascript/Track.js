// Track object

var Track = function( precision, dbg_msg ) {

  this.precision = precision;
  this.dbg_msg = dbg_msg;
  this.deg2rad = Math.PI/180.0;

  this.name = '';					// raceTrack information
  this.id = '';

  this.race = {						// raceSession information
    laps: 0,
    maxLapTimeMs: 0,
    quickRace: false
  };

  this.lanes = [];				// { index: , distanceFromCenter: }
  this.iLanes = [];				// just the indexes
  this.iSwitches = [];		// piece indexes for switchpiece locations

  this.numLanes = 0;			// number of lanes on this track
  this.laneLengths = [];	// calculated lengths for each lane on a piece

  this.pieces = [];				// pieces as they come from server
  this.lapModulo = 0;			// amount of pieces ( modulo this = lap )
  this.switchModulo = 0;	// amount of switches ( modulo this = next lap 1st switch )

};

var proto = Track.prototype;

proto.gameInit = function( d ) {
  // Processes the gameInit message (track info)

  this.id = d.track.id;
  this.name = d.track.name;
  this.race.laps = d.raceSession.laps;
  this.race.maxLapTimeMs = d.raceSession.maxLapTimeMs;
  if ( d.raceSession.hasOwnProperty( 'quickRace' )) {
    this.race.quickRace = d.raceSession.raceSession;
  } else {
    this.race.quickRace = false;
  }

  this.processPieces( d.track );
};


proto.processPieces = function( d ) {
  // preprocesses all pieces in the track
  // first ensure that lanes are sorted from left to right!
  this.lanes = d.lanes;
  this.lanes.sort( function(a, b) { return a.distanceFromCenter - b.distanceFromCenter; } );
  this.numLanes = this.lanes.length;

  var i = 0; // To ensure i stays in local scope

  // lanes are sorted, iLanes has just their numerical indexes, lanes are objects.
  for ( i = 0; i < this.lanes.length; i++ ) {
    this.iLanes.push( this.lanes[i].index );    
  }

  // store the original track pieces
  this.pieces = d.pieces;
  this.lapModulo = this.pieces.length;

  // calculate lane lengths for each piece and store them in our own data structure
  // also separate switchpieces to their own structure
  for ( i = 0; i < this.lapModulo; i++ ) {
    if ( this.pieces[i].hasOwnProperty( 'switch' ) ) {
      if ( this.pieces[i].switch ) {
        this.iSwitches.push( i );  
      }
    }
    this.laneLengths.push( this.calculateLaneLengths( i ) );
  }

  this.swicthModulo = this.iSwitches.length;

};

proto.getShortestLanes = function ( pieceIndex, currentLane, leftLanes, rightLanes ) {
  // returns shortest lanes from next switch index
  // ( r[0] = shortest. use r[0].dir as value for switchLane in car )
  var si1 = this.getNextSwitchIndex( pieceIndex );
  var si2 = this.getNextSwitchIndex( ( si1 + 1 ) % this.lapModulo );
  var r = this.getLaneLengthsBetweenSwitches( si1, si2, currentLane, leftLanes, rightLanes )
  return r;
}

proto.getLaneInfo = function( pieceIndex, currentLane ) {
  pieceIndex = pieceIndex % this.lapModulo;
  // TODO: make currentlane robust too?
  var piece = this.laneLengths[pieceIndex];
  var lane = piece[currentLane];
  return lane;
}

proto.nextPieceIndex = function ( pieceIndex ) {
  return ( pieceIndex + 1 ) % this.lapModulo;
}

proto.getNextDisturbance = function ( pieceIndex, currentLane, inPieceDistance ) {
  // get the next disturbance (change in radius)
  // and return its distance, change to current radius, absolute magnitude, angle
  var lane = this.getLaneInfo( pieceIndex, currentLane );
  var currentRadius = lane.radius;
  var distance = lane.keep - inPieceDistance;
  var nextRadius = 0;
  while ( true ) {
    pieceIndex = this.nextPieceIndex( pieceIndex );
    lane = this.getLaneInfo( pieceIndex, currentLane );
    nextRadius = lane.radius;
    if ( nextRadius != currentRadius ) {
      return [ distance, nextRadius - currentRadius, nextRadius, lane.angle, lane.keep ];
    }
    distance += lane.keep;
  }
}

proto.getAverageRadius = function() {
  // calculates average radius of all radiuses
  var sum = 0;
  var num = 0;
  for ( var i = 0; i < this.laneLengths.length; i++) {
    for ( var j = 0; j < this.laneLengths[i].length; j++) {
      if ( this.laneLengths[i][j].radius > 0 ) {
        num += 1;
        sum += this.laneLengths[i][j].radius;
      }
    }
  }
  return sum/num;
}

proto.getTightness = function ( distance, pieceIndex, currentLane, inPieceDistance ) {
  // calculates tightness of upcoming curve
  var d = this.laneLengths[pieceIndex][currentLane].keep - inPieceDistance;
  var a = this.laneLengths[pieceIndex][currentLane].radius;
  var r = a * distance;
  if ( distance < d ) {
    return r;
  }
  r = a * d;
  distance = distance - d;
  while ( true ) {
    pieceIndex = ( pieceIndex + 1 ) % this.lapModulo;
    d = this.laneLengths[pieceIndex][currentLane].keep;
    a = this.laneLengths[pieceIndex][currentLane].angle;
    if ( distance <= d ) {
      r += a * distance;
      return r;
    }
    r += a * d;
    distance = distance - d;
  }
}



proto.getNextSwitchIndex = function ( pieceIndex ) {
  // we assume that there is always at least one switchindex present!
  while ( true ) {
    if ( this.laneLengths[pieceIndex][0].switchPiece ) {
      return pieceIndex;
    }
    pieceIndex = ( pieceIndex + 1 ) % this.lapModulo;
  }
}

proto.getKeepLength = function ( startIndex, endIndex, lane ) {
  // calculates length from startIndex (including it)
  // to endIndex (excluding it)

  var keepLength = 0;

  while ( startIndex != endIndex) {
    keepLength = keepLength + this.laneLengths[startIndex][lane].keep;
    startIndex = (startIndex + 1) % this.lapModulo;
  }

  return keepLength;
}

proto.getDistanceToNextCurve = function ( pieceIndex, inPieceDistance, currentLane ) {
  // returns [ distance to next curvePiece, lanedata for that ].
  var d = this.laneLengths[pieceIndex][currentLane].keep - inPieceDistance;
  while ( true ) {
    pieceIndex = ( pieceIndex + 1) % this.lapModulo;
    if ( this.laneLengths[pieceIndex][currentLane].radius != 0 ) {
      //console.log('radius value: %s', this.laneLengths[pieceIndex][currentLane].radius);
      return [ d, this.laneLengths[pieceIndex][currentLane] ];
    } else {
      d += this.laneLengths[pieceIndex][currentLane].keep;
    }
  }
}

proto.getLaneLengthsBetweenSwitches = function ( startSwitch, endSwitch, currentLane, leftLanes, rightLanes ) {
  // sorts routes ( shortest first, dir: false | 'Left' | 'Right' )
  var r = [];
  
  r.push({
    dir: false,
    len: this.getKeepLength( startSwitch, endSwitch, currentLane )
  });
  
  if ( leftLanes.length > 0 ) {
    r.push({
      dir: 'Left',
      len: this.getKeepLength( (startSwitch + 1) % this.lapModulo, endSwitch, leftLanes[leftLanes.length-1] ) + this.laneLengths[startSwitch][currentLane].left 
    });
  }

  if ( rightLanes.length > 0 ) {
    r.push({
      dir: 'Right',
      len: this.getKeepLength( (startSwitch+1) % this.lapModulo, endSwitch, rightLanes[0] ) + this.laneLengths[startSwitch][currentLane].right
    });
  }

  r.sort( function(a, b) { return a.len - b.len; });
  
  return r;  
}


proto.calculateLaneLengths = function( pieceIndex ) {
  // returns all lane lengths of given pieceIndex
  
  var r = [];
  var p = this.pieces[pieceIndex];
  var i = 0;

  var c = 0, cl = 0, cr = 0; 	// curve lengths
  var sll = 0, slr = 0;				// shift lengths
  var ldl = 0, ldr = 0; 			// lane distances

  // straight piece with or without switch
  if ( p.hasOwnProperty('length') ) {
    for ( i = 0; i < this.numLanes; i++ ) {
      if ( p.hasOwnProperty('switch') ) {   

        if (this.lanes[i-1]) {
          ldl = Math.abs(this.lanes[i-1].distanceFromCenter - this.lanes[i].distanceFromCenter);
          sll = Math.sqrt(Math.pow(ldl, 2) + Math.pow(p.length, 2));
        }

        if (this.lanes[i+1]) {
          ldr = Math.abs(this.lanes[i+1].distanceFromCenter - this.lanes[i].distanceFromCenter);
          slr = Math.sqrt(Math.pow(ldr, 2) + Math.pow(p.length, 2));
        }

        if (i === 0) {
          r.push({ angle: 0, radius: 0, switchPiece: true, keep: p.length, right: slr });
        } else if (i == (this.numLanes - 1)) {
          r.push({ angle: 0, radius: 0, switchPiece: true, keep: p.length, left: sll });
        } else {
          r.push({ angle: 0, radius: 0, switchPiece: true, keep: p.length, left: sll, right: slr });
        }
      } else {
        r.push({ angle: 0, radius: 0, switchPiece: false, keep: p.length });
      }   
    }
  } else {
    // curved piece with or without switch
    var radius = 0, rl = 0, rr = 0;
    for ( i = 0; i < this.numLanes; i++ ) {

      if (p.angle > 0) {
        radius = p.radius - this.lanes[i].distanceFromCenter;

      } else {
        radius = p.radius + this.lanes[i].distanceFromCenter;  
      }
      c = (Math.abs(p.angle)*this.deg2rad) * radius;

      if ( p.hasOwnProperty('switch') ) {

        if (this.lanes[i-1]) {
          ldl = Math.abs(this.lanes[i-1].distanceFromCenter - this.lanes[i].distanceFromCenter);
        }

        if (this.lanes[i+1]) {
          ldr = Math.abs(this.lanes[i+1].distanceFromCenter - this.lanes[i].distanceFromCenter);
        }

        if (p.angle > 0) {
          rl = radius - ldl;
          rr = radius - ldr;

        } else {
          rl = radius + ldl;
          rr = radius + ldr;
        }

        cl = (Math.abs(p.angle)*this.deg2rad) * rl;
        cr = (Math.abs(p.angle)*this.deg2rad) * rr;
        
        sll = (c + cl) / 2;
        slr = (c + cr) / 2;

        if (i === 0) {
          r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, right: slr });
        } else if (i == (this.numLanes - 1)) {
          r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, left: sll, right: slr });
        } else {
          r.push({ angle: p.angle, radius: radius, switchPiece: true, keep: c, left: sll, right: slr });
        }
      } else {
        r.push({ angle: p.angle, radius: radius, switchPiece: false, keep: c });
      }
    }
  }
  return r;
};



exports.Track = Track;
